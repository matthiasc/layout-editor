/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#include "config.h"

#include "size-editor.h"

struct _SizeEditor
{
  GtkWidget parent;

  GtkSpinButton *editor;
  GtkToggleButton *px_mode;
};

struct _SizeEditorClass
{
  GtkWidgetClass parent_class;
};

enum {
  PROP_VALUE = 1,
  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL };

/* {{{ Editor implementation */

static void
value_changed (GtkWidget  *widget,
               GParamSpec *pspec,
               gpointer    data)
{
  g_object_notify_by_pspec (G_OBJECT (data), properties[PROP_VALUE]);
}

static void
px_changed (GtkWidget *widget,
            GParamSpec *pspec,
            gpointer    data)
{
  SizeEditor *self = data;
  double value;

  value = gtk_spin_button_get_value (self->editor);
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
    gtk_spin_button_set_value (self->editor, value / PANGO_SCALE);
  else
    gtk_spin_button_set_value (self->editor, value * PANGO_SCALE);
}

/*  }}} */
/* {{{ GObject boilerplate */

G_DEFINE_TYPE (SizeEditor, size_editor, GTK_TYPE_WIDGET);

static void
size_editor_init (SizeEditor *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
size_editor_finalize (GObject *object)
{
  //SizeEditor *self = (SizeEditor *)object;

  G_OBJECT_CLASS (size_editor_parent_class)->finalize (object);
}

static void
size_editor_dispose (GObject *object)
{
  SizeEditor *self = (SizeEditor *)object;

  g_clear_pointer ((GtkWidget **)&self->editor, gtk_widget_unparent);
  g_clear_pointer ((GtkWidget **)&self->px_mode, gtk_widget_unparent);

  G_OBJECT_CLASS (size_editor_parent_class)->dispose (object);
}

static void
size_editor_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  SizeEditor *self = (SizeEditor *)object;

  switch (property_id)
    {
    case PROP_VALUE:
      size_editor_set_value (self, g_value_get_int (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
size_editor_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  SizeEditor *self = (SizeEditor *)object;

  switch (property_id)
    {
    case PROP_VALUE:
      g_value_set_int (value, size_editor_get_value (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
size_editor_class_init (SizeEditorClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  object_class->finalize = size_editor_finalize;
  object_class->dispose = size_editor_dispose;
  object_class->set_property = size_editor_set_property;
  object_class->get_property = size_editor_get_property;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/pango/LayoutEditor/size-editor.ui");

  gtk_widget_class_bind_template_child (widget_class, SizeEditor, editor);
  gtk_widget_class_bind_template_child (widget_class, SizeEditor, px_mode);

  gtk_widget_class_bind_template_callback (widget_class, value_changed);
  gtk_widget_class_bind_template_callback (widget_class, px_changed);

  properties[PROP_VALUE] =
    g_param_spec_int ("value", NULL, NULL,
                      G_MININT, G_MAXINT, 0,
                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BOX_LAYOUT);

  gtk_widget_class_set_css_name (widget_class, "size-editor");
}

/* }}} */
/* {{{ Public API */

SizeEditor *
size_editor_new (void)
{
  return g_object_new (size_editor_get_type (), NULL);
}

void
size_editor_set_value (SizeEditor *self,
                       int         value)
{
  if (gtk_toggle_button_get_active (self->px_mode))
    gtk_spin_button_set_value (self->editor, value / PANGO_SCALE);
  else
    gtk_spin_button_set_value (self->editor, value);
}

int
size_editor_get_value (SizeEditor *self)
{
  if (gtk_toggle_button_get_active (self->px_mode))
    return gtk_spin_button_get_value (self->editor) * PANGO_SCALE;
  else
    return gtk_spin_button_get_value (self->editor);
}

/* }}} */

/* vim:set foldmethod=marker expandtab: */
