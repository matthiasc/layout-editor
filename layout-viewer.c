/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#include "config.h"

#include <pango/pangoft2.h>

#include "layout-viewer.h"

struct _LayoutViewer
{
  GtkWidget parent;
  GtkPopover *run_info;
  GtkLabel *bytes_label;
  GtkLabel *chars_label;
  GtkLabel *text_label;
  GtkLabel *bidi_label;
  GtkLabel *gravity_label;
  GtkLabel *flags_label;
  GtkLabel *font_label;
  GtkLabel *font_file_label;
  GtkLabel *script_label;
  GtkLabel *language_label;
  GtkLabel *attributes_label;

  PangoLayoutRun *highlight_run;
  PangoLayout *layout;

  gboolean use_cairo;
  gboolean show_bounds;
  gboolean show_highlight;
  gboolean show_layout_extents;
  gboolean show_tab_positions;
  gboolean show_line_extents;
  gboolean show_run_extents;
  gboolean show_cluster_extents;
  gboolean show_char_extents;
  gboolean show_glyph_extents;
  gboolean show_caret_positions;
  gboolean show_caret_slope;
  gboolean show_sentence_boundaries;
  gboolean show_word_boundaries;
  gboolean show_grapheme_boundaries;
  gboolean show_break_positions;

  guint tick_cb;
  guint serial;
};

struct _LayoutViewerClass
{
  GtkWidgetClass parent_class;
};

/* {{{ Viewer implementation */

static gboolean
tick_cb (GtkWidget     *widget,
         GdkFrameClock *clock,
         gpointer       user_data)
{
  LayoutViewer *self = (LayoutViewer *)widget;
  guint serial;

  serial = pango_layout_get_serial (self->layout);
  if (self->serial != serial)
    {
      gtk_popover_popdown (self->run_info);
      self->show_highlight = FALSE;
      self->serial = serial;
      gtk_widget_queue_resize (widget);
    }

  return G_SOURCE_CONTINUE;
}

static void
update_from_layout (LayoutViewer *self)
{
  gtk_widget_queue_resize (GTK_WIDGET (self));

  if (!self->layout)
    {
      if (self->tick_cb)
        gtk_widget_remove_tick_callback (GTK_WIDGET (self), self->tick_cb);
      self->tick_cb = 0;
    }
  else
    {
      self->serial = 0;
      if (!self->tick_cb)
        self->tick_cb = gtk_widget_add_tick_callback (GTK_WIDGET (self), tick_cb, NULL, NULL);
    }
}

static void
draw_tab (cairo_t *cr,
          int      pos,
          int      alignment)
{
  switch (alignment)
    {
    case PANGO_TAB_LEFT:
      cairo_move_to (cr, pos, - 7.5);
      cairo_line_to (cr, pos, - 2.5);
      cairo_line_to (cr, pos - 5., - 2.5);
      cairo_stroke (cr);
      break;
    case PANGO_TAB_RIGHT:
      cairo_move_to (cr, pos, - 7.5);
      cairo_line_to (cr, pos, - 2.5);
      cairo_line_to (cr, pos + 5., - 2.5);
      cairo_stroke (cr);
      break;
    case PANGO_TAB_CENTER:
      cairo_move_to (cr, pos, - 7.5);
      cairo_line_to (cr, pos, - 2.5);
      cairo_stroke (cr);
      cairo_move_to (cr, pos - 5., - 2.5);
      cairo_line_to (cr, pos + 5., - 2.5);
      cairo_stroke (cr);
      break;
    case PANGO_TAB_DECIMAL:
      cairo_move_to (cr, pos, - 5.5);
      cairo_line_to (cr, pos, - 2.5);
      cairo_stroke (cr);
      cairo_move_to (cr, pos - 5., - 2.5);
      cairo_line_to (cr, pos + 5., - 2.5);
      cairo_stroke (cr);
      cairo_arc (cr, pos, -7.5, 1., 0, 2 * G_PI);
      cairo_fill (cr);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
layout_viewer_snapshot (GtkWidget   *widget,
                        GtkSnapshot *snapshot)
{
  LayoutViewer *self = (LayoutViewer *) widget;
  GtkStyleContext *context;
  GdkRGBA color;
  int w, h;
  int layout_set_width;
  int layout_width, layout_height;
  PangoRectangle ink, logical;

  if (!self->layout)
    return;

  w = gtk_widget_get_width (widget);
  h = gtk_widget_get_height (widget);

  pango_layout_get_extents (self->layout, &ink, &logical);

  layout_set_width = pango_layout_get_width (self->layout);
  pango_layout_get_size (self->layout, &layout_width, &layout_height);

  context = gtk_widget_get_style_context (widget);
  gtk_style_context_get_color (context, &color);

  gtk_snapshot_save (snapshot);

  if (self->show_bounds ||
      self->show_highlight ||
      self->show_layout_extents ||
      self->show_tab_positions ||
      self->show_line_extents ||
      self->show_run_extents ||
      self->show_cluster_extents ||
      self->show_char_extents ||
      self->show_glyph_extents ||
      self->show_caret_positions ||
      self->show_caret_slope ||
      self->show_sentence_boundaries ||
      self->show_word_boundaries ||
      self->show_grapheme_boundaries ||
      self->show_break_positions)
    {
      cairo_t *cr;
      const PangoMatrix *matrix;

      cr = gtk_snapshot_append_cairo (snapshot, &GRAPHENE_RECT_INIT (-10, -10, w + 20, h + 20));

      cairo_set_line_width (cr, 1.);

      matrix = pango_context_get_matrix (pango_layout_get_context (self->layout));
      if (matrix)
        {
          cairo_matrix_t cairo_matrix;
          PangoRectangle rect;

          rect = (PangoRectangle) { 0, 0, MAX (layout_set_width, layout_width), layout_height };
          pango_matrix_transform_rectangle (matrix, &rect);

          cairo_matrix.xx = matrix->xx;
          cairo_matrix.yx = matrix->yx;
          cairo_matrix.xy = matrix->xy;
          cairo_matrix.yy = matrix->yy;
          cairo_matrix.x0 = matrix->x0 - rect.x / PANGO_SCALE;
          cairo_matrix.y0 = matrix->y0 - rect.y / PANGO_SCALE;

          cairo_set_matrix (cr, &cairo_matrix);
        }

      if (self->show_bounds)
        {
          cairo_save (cr);
          if (layout_set_width > -1)
            {
              cairo_set_source_rgba (cr, 1., 0., 0., 1.);
              cairo_rectangle (cr, - 0.5, - 0.5, PANGO_PIXELS_CEIL (layout_set_width) + 1., PANGO_PIXELS_CEIL (layout_height) + 1.);
              cairo_stroke (cr);
            }

          cairo_set_source_rgba (cr, color.red, color.green, color.blue, color.alpha);
          cairo_rectangle (cr, - 0.5, - 0.5, PANGO_PIXELS_CEIL (MAX (layout_set_width, layout_width)) + 1., PANGO_PIXELS_CEIL (layout_height) + 1.);
          cairo_stroke (cr);
          cairo_restore (cr);
        }

      if (self->show_tab_positions)
        {
          PangoTabArray *tabs = pango_layout_get_tabs (self->layout);
          PangoTabAlign alignment = PANGO_TAB_LEFT;
          int pos = 0;
          int gap = 0;

          if (tabs)
            {
              int prev_pos;

              cairo_save (cr);
              cairo_set_source_rgba (cr, color.red, color.green, color.blue, color.alpha);

              for (int i = 0; i < pango_tab_array_get_size (tabs); i++)
                {
                  prev_pos = pos;
                  pango_tab_array_get_tab (tabs, i, &alignment, &pos);
                  if (!pango_tab_array_get_positions_in_pixels (tabs))
                    pos = PANGO_PIXELS_CEIL (pos);

                  if (pos > MAX (layout_set_width, layout_width))
                    break;

                  cairo_move_to (cr, pos, 0.);
                  cairo_line_to (cr, pos, PANGO_PIXELS_CEIL (layout_height));
                  cairo_stroke (cr);
                  draw_tab (cr, pos, alignment);
                }

              cairo_set_source_rgba (cr, color.red, color.green, color.blue, color.alpha / 2);

              gap = pos - prev_pos;

              pango_tab_array_free (tabs);
            }
          else
            {
              PangoLayoutLine *line;
              PangoLayoutRun *run;
              PangoGlyphString *glyphs;

              line = pango_layout_get_line (self->layout, 0);
              run = line->runs->data;
              glyphs = pango_glyph_string_new ();
              pango_shape ("        ", 8, &run->item->analysis, glyphs);
              gap = PANGO_PIXELS_CEIL (pango_glyph_string_get_width (glyphs));
              pango_glyph_string_free (glyphs);
            }

          if (gap > 0)
            {
              pos += gap;
              for (; pos < MAX (layout_set_width, layout_width); pos += gap)
                {
                  cairo_move_to (cr, pos, 0.);
                  cairo_line_to (cr, pos, PANGO_PIXELS_CEIL (layout_height));
                  cairo_stroke (cr);
                  draw_tab (cr, pos, alignment);
                }
            }

          cairo_restore (cr);
        }

      if (self->show_layout_extents)
        {
          cairo_save (cr);
          cairo_set_source_rgba (cr, 1., 0., 0., 0.5);

          cairo_rectangle (cr,
                           (double)logical.x / PANGO_SCALE - 0.5,
                           (double)logical.y / PANGO_SCALE - 0.5,
                           (double)logical.width / PANGO_SCALE + 1.,
                           (double)logical.height / PANGO_SCALE + 1.);
          cairo_stroke (cr);
          cairo_restore (cr);

          cairo_save (cr);
          cairo_set_source_rgba (cr, 0., 1., 0., 0.5);
          cairo_rectangle (cr,
                           (double)ink.x / PANGO_SCALE - 0.5,
                           (double)ink.y / PANGO_SCALE - 0.5,
                           (double)ink.width / PANGO_SCALE + 1.,
                           (double)ink.height / PANGO_SCALE + 1.);
          cairo_stroke (cr);

          cairo_restore (cr);
        }

      if (self->show_line_extents)
        {
          PangoLayoutIter *iter;

          cairo_save (cr);
          cairo_set_source_rgba (cr, 1., 0., 0., 0.5);

          iter = pango_layout_get_iter (self->layout);
          do
            {
              PangoRectangle rect;

              pango_layout_iter_get_line_extents (iter, NULL, &rect);
              cairo_rectangle (cr,
                               (double)rect.x / PANGO_SCALE - 0.5,
                               (double)rect.y / PANGO_SCALE - 0.5,
                               (double)rect.width / PANGO_SCALE + 1.,
                               (double)rect.height / PANGO_SCALE + 1.);
              cairo_stroke (cr);
            }
          while (pango_layout_iter_next_line (iter));
          pango_layout_iter_free (iter);
          cairo_restore (cr);
        }

      if (self->show_run_extents ||
          self->show_highlight)
        {
          PangoLayoutIter *iter;

          cairo_save (cr);

          iter = pango_layout_get_iter (self->layout);
          do
            {
              PangoLayoutRun *run;
              PangoRectangle rect;

              run = pango_layout_iter_get_run (iter);
              if (!run)
                continue;

              pango_layout_iter_get_run_extents (iter, NULL, &rect);
              if (self->show_run_extents)
                {
                  cairo_set_source_rgba (cr, 0., 0., 1., .5);
                  cairo_rectangle (cr,
                                   (double)rect.x / PANGO_SCALE - 0.5,
                                   (double)rect.y / PANGO_SCALE - 0.5,
                                   (double)rect.width / PANGO_SCALE + 1.,
                                   (double)rect.height / PANGO_SCALE + 1.);
                  cairo_stroke (cr);
                }

              if (self->show_highlight && run == self->highlight_run)
                {
                  cairo_set_source_rgba (cr, 0.2, 0.2, 0.9, 0.3);
                  cairo_rectangle (cr,
                                   (double)rect.x / PANGO_SCALE - 0.5,
                                   (double)rect.y / PANGO_SCALE - 0.5,
                                   (double)rect.width / PANGO_SCALE + 1.,
                                   (double)rect.height / PANGO_SCALE + 1.);
                  cairo_fill (cr);
                }
            }
          while (pango_layout_iter_next_run (iter));
          pango_layout_iter_free (iter);
          cairo_restore (cr);
        }

      if (self->show_cluster_extents)
        {
          PangoLayoutIter *iter;

          cairo_save (cr);
          cairo_set_source_rgba (cr, 1., 0., 1., .5);

          iter = pango_layout_get_iter (self->layout);
          do
            {
              PangoRectangle rect;

              pango_layout_iter_get_cluster_extents (iter, NULL, &rect);
              cairo_rectangle (cr,
                               (double)rect.x / PANGO_SCALE - 0.5,
                               (double)rect.y / PANGO_SCALE - 0.5,
                               (double)rect.width / PANGO_SCALE + 1.,
                               (double)rect.height / PANGO_SCALE + 1.);
              cairo_stroke (cr);
            }
          while (pango_layout_iter_next_cluster (iter));
          pango_layout_iter_free (iter);
          cairo_restore (cr);
        }

      if (self->show_char_extents)
        {
          PangoLayoutIter *iter;

          cairo_save (cr);
          cairo_set_source_rgba (cr, 1., 0.5, 0., .5);

          iter = pango_layout_get_iter (self->layout);
          do
            {
              PangoRectangle rect;

              pango_layout_iter_get_char_extents (iter, &rect);
              cairo_rectangle (cr,
                               (double)rect.x / PANGO_SCALE - 0.5,
                               (double)rect.y / PANGO_SCALE - 0.5,
                               (double)rect.width / PANGO_SCALE + 1.,
                               (double)rect.height / PANGO_SCALE + 1.);
              cairo_stroke (cr);
            }
          while (pango_layout_iter_next_char (iter));
          pango_layout_iter_free (iter);
          cairo_restore (cr);
        }

      if (self->show_glyph_extents)
        {
          PangoLayoutIter *iter;

          cairo_save (cr);
          cairo_set_source_rgba (cr, 0., 0., 1., .5);

          iter = pango_layout_get_iter (self->layout);
          do
            {
              PangoLayoutRun *run;
              PangoRectangle rect;
              int x_pos, y_pos;

              run = pango_layout_iter_get_run (iter);
              if (!run)
                continue;

              pango_layout_iter_get_run_extents (iter, NULL, &rect);

              x_pos = rect.x;
              y_pos = pango_layout_iter_get_run_baseline (iter);

              for (int i = 0; i < run->glyphs->num_glyphs; i++)
                {
                  PangoRectangle extents;

                  pango_font_get_glyph_extents (run->item->analysis.font,
                                                run->glyphs->glyphs[i].glyph,
                                                &extents, NULL);

                  rect.x = x_pos + run->glyphs->glyphs[i].geometry.x_offset + extents.x;
                  rect.y = y_pos + run->glyphs->glyphs[i].geometry.y_offset + extents.y;
                  rect.width = extents.width;
                  rect.height = extents.height;

                  cairo_rectangle (cr,
                                   (double)rect.x / PANGO_SCALE - 0.5,
                                   (double)rect.y / PANGO_SCALE - 0.5,
                                   (double)rect.width / PANGO_SCALE + 1.,
                                   (double)rect.height / PANGO_SCALE + 1.);
                  cairo_stroke (cr);

                  cairo_arc (cr,
                             (double) (x_pos + run->glyphs->glyphs[i].geometry.x_offset) / PANGO_SCALE,
                             (double) (y_pos + run->glyphs->glyphs[i].geometry.y_offset) / PANGO_SCALE,
                             3.0, 0, 2*G_PI);
                  cairo_fill (cr);

                  x_pos += run->glyphs->glyphs[i].geometry.width;
                }
            }
          while (pango_layout_iter_next_run (iter));
          pango_layout_iter_free (iter);
          cairo_restore (cr);
        }

      if (self->show_caret_positions)
        {
          PangoLayoutIter *iter;
          const PangoLogAttr *attrs;
          int n_attrs;
          int offset;
          int num = 0;

          cairo_save (cr);
          cairo_set_source_rgba (cr, 1., 0., 1., 0.5);

          attrs = pango_layout_get_log_attrs_readonly (self->layout, &n_attrs);

          iter = pango_layout_get_iter (self->layout);
          do
            {
              PangoRectangle rect;
              PangoLayoutRun *run;
              const char *text, *start, *p;
              int x, y;
              gboolean trailing;

              pango_layout_iter_get_run_extents (iter, NULL, &rect);
              run = pango_layout_iter_get_run_readonly (iter);

              if (!run)
                continue;

              text = pango_layout_get_text (self->layout);
              start = text + run->item->offset;

              offset = g_utf8_strlen (text, start - text);

              y = pango_layout_iter_get_run_baseline (iter);
              trailing = FALSE;
              p = start;
              for (int i = 0; i <= run->item->num_chars; i++)
                {
                  if (attrs[offset + i].is_cursor_position)
                    {
                      pango_glyph_string_index_to_x_full (run->glyphs,
                                                          text + run->item->offset,
                                                          run->item->length,
                                                          &run->item->analysis,
                                                          (PangoLogAttr *)attrs + offset,
                                                          p - start,
                                                          trailing,
                                                          &x);
                      x += rect.x;

                      cairo_set_source_rgba (cr, 1., 0., 1., 0.5);
                      cairo_arc (cr, x / PANGO_SCALE, y / PANGO_SCALE, 3., 0, 2*G_PI);
                      cairo_close_path (cr);
                      cairo_fill (cr);

                      char *s = g_strdup_printf ("%d", num);
                      cairo_set_source_rgba (cr, color.red, color.green, color.blue, color.alpha);
                      cairo_move_to (cr, x / PANGO_SCALE - 5, y / PANGO_SCALE + 15);
                      cairo_show_text (cr, s);
                      g_free (s);
                   }

                  if (i < run->item->num_chars)
                    {
                      num++;
                      p = g_utf8_next_char (p);
                    }
                  else
                    trailing = TRUE;

                }
            }
          while (pango_layout_iter_next_run (iter));
          pango_layout_iter_free (iter);
          cairo_restore (cr);
        }

      if (self->show_caret_slope)
        {
          const char *text = pango_layout_get_text (self->layout);
          int length = g_utf8_strlen (text, -1);
          const PangoLogAttr *attrs;
          int n_attrs;
          const char *p;
          int i;

          cairo_save (cr);
          cairo_set_source_rgba (cr, color.red, color.green, color.blue, 0.5);

          attrs = pango_layout_get_log_attrs_readonly (self->layout, &n_attrs);

          for (i = 0, p = text; i <= length; i++, p = g_utf8_next_char (p))
            {
              PangoRectangle rect;

              if (!attrs[i].is_cursor_position)
                continue;

              pango_layout_get_caret_pos (self->layout, p - text, &rect, NULL);

              cairo_move_to (cr,
                             (double)rect.x / PANGO_SCALE + (double)rect.width / PANGO_SCALE - 0.5,
                             (double)rect.y / PANGO_SCALE - 0.5);
              cairo_line_to (cr,
                             (double)rect.x / PANGO_SCALE - 0.5,
                             (double)rect.y / PANGO_SCALE + (double)rect.height / PANGO_SCALE - 0.5);
              cairo_stroke (cr);
            }

          cairo_restore (cr);
        }

      if (self->show_sentence_boundaries ||
          self->show_word_boundaries ||
          self->show_grapheme_boundaries ||
          self->show_break_positions)
        {
          const char *text = pango_layout_get_text (self->layout);
          int length = g_utf8_strlen (text, -1);
          const PangoLogAttr *attrs;
          int n_attrs;
          const char *p;
          int i;

          cairo_save (cr);
          cairo_set_source_rgba (cr, color.red, color.green, color.blue, 0.5);

          attrs = pango_layout_get_log_attrs_readonly (self->layout, &n_attrs);

          for (i = 0, p = text; i <= length; i++, p = g_utf8_next_char (p))
            {
              PangoRectangle rect;

              if (!((self->show_sentence_boundaries && attrs[i].is_sentence_boundary) ||
                    (self->show_word_boundaries && attrs[i].is_word_boundary) ||
                    (self->show_grapheme_boundaries && attrs[i].is_cursor_position) ||
                    (self->show_break_positions && attrs[i].is_line_break)))
                continue;

              pango_layout_index_to_pos (self->layout, p - text, &rect);

              cairo_move_to (cr,
                             (double)rect.x / PANGO_SCALE - 0.5,
                             (double)rect.y / PANGO_SCALE - 0.5);
              cairo_line_to (cr,
                             (double)rect.x / PANGO_SCALE - 0.5,
                             (double)rect.y / PANGO_SCALE + (double)rect.height / PANGO_SCALE - 0.5);
              cairo_stroke (cr);

            }

          cairo_restore (cr);
        }

      cairo_destroy (cr);
    }

  if (self->use_cairo)
    {
      cairo_t *cr;
      const PangoMatrix *matrix;

      cr = gtk_snapshot_append_cairo (snapshot, &GRAPHENE_RECT_INIT (0, 0, w, h));

      matrix = pango_context_get_matrix (pango_layout_get_context (self->layout));
      if (matrix)
        {
          cairo_matrix_t cairo_matrix;
          PangoRectangle rect;

          rect = (PangoRectangle) { 0, 0, MAX (layout_set_width, layout_width), layout_height };
          pango_matrix_transform_rectangle (matrix, &rect);

          cairo_matrix.xx = matrix->xx;
          cairo_matrix.yx = matrix->yx;
          cairo_matrix.xy = matrix->xy;
          cairo_matrix.yy = matrix->yy;
          cairo_matrix.x0 = matrix->x0 - rect.x / PANGO_SCALE;
          cairo_matrix.y0 = matrix->y0 - rect.y / PANGO_SCALE;

          cairo_set_matrix (cr, &cairo_matrix);
        }

      cairo_set_source_rgba (cr, 1., 1., 1., 0.);
      cairo_paint (cr);
      cairo_set_source_rgba (cr, color.red, color.green, color.blue, color.alpha);
      pango_cairo_show_layout (cr, self->layout);

      cairo_surface_flush (cairo_get_target (cr));

      cairo_destroy (cr);
    }
  else
    {
      const PangoMatrix *matrix;

      matrix = pango_context_get_matrix (pango_layout_get_context (self->layout));
      if (matrix)
        {
          graphene_matrix_t *m;
          float v[16];
          PangoRectangle rect;

          rect = (PangoRectangle) { 0, 0, MAX (layout_set_width, layout_width), layout_height };
          pango_matrix_transform_rectangle (matrix, &rect);

          v[0]  = matrix->xx; v[1]  = matrix->yx; v[2]  = 0.; v[3]  = 0.;
          v[4]  = matrix->xy; v[5]  = matrix->yy; v[6]  = 0.; v[7]  = 0.;
          v[8]  = 0.;         v[9]  = 0.;         v[10] = 1.; v[11] = 0.;
          v[12] = matrix->x0 - rect.x / PANGO_SCALE; v[13] = matrix->y0 - rect.y / PANGO_SCALE; v[14] = 0.; v[15] = 1.;

          m = graphene_matrix_alloc ();
          graphene_matrix_init_from_float (m, v);
          gtk_snapshot_transform_matrix (snapshot, m);
          graphene_matrix_free (m);
        }

      gtk_snapshot_append_layout (snapshot, self->layout, &color);
    }

  gtk_snapshot_restore (snapshot);
}

static gboolean
pango_rectangle_contains_point (const PangoRectangle *rect,
                                int                   x,
                                int                   y)
{
  return x >= rect->x &&
         x < rect->x + rect->width &&
         y >= rect->y &&
         y < rect->y + rect->height;
}

static void
pango_matrix_invert (PangoMatrix *m)
{
  cairo_matrix_t cm;

  cm.xx = m->xx; cm.xy = m->xy;
  cm.yx = m->yx; cm.yy = m->yy;
  cm.x0 = m->x0; cm.y0 = m->y0;

  cairo_matrix_invert (&cm);

  m->xx = cm.xx; m->xy = cm.xy;
  m->yx = cm.yx; m->yy = cm.yy;
  m->x0 = cm.x0; m->y0 = cm.y0;
}

static PangoLayoutRun *
find_run_at_pos (PangoLayout     *layout,
                 int              x,
                 int              y,
                 GdkRectangle    *bounds)
{
  PangoLayoutIter *iter;
  PangoLayoutRun *result = NULL;
  const PangoMatrix *matrix;
  PangoMatrix m = PANGO_MATRIX_INIT;
  double px = x;
  double py = y;

  matrix = pango_context_get_matrix (pango_layout_get_context (layout));
  if (matrix)
    {
      PangoRectangle rect;
      int layout_set_width, layout_width, layout_height;
      PangoMatrix minv;

      layout_set_width = pango_layout_get_width (layout);
      pango_layout_get_size (layout, &layout_width, &layout_height);

      m = *matrix;

      rect = (PangoRectangle) { 0, 0, MAX (layout_set_width, layout_width), layout_height };
      pango_matrix_transform_rectangle (&m, &rect);

      m.x0 = matrix->x0 - rect.x / PANGO_SCALE;
      m.y0 = matrix->y0 - rect.y / PANGO_SCALE;

      minv = m;
      pango_matrix_invert (&minv);
      pango_matrix_transform_point (&minv, &px, &py);
    }

  iter = pango_layout_get_iter (layout);

  do {
    PangoLayoutRun *run;
    PangoRectangle rect;

    run = pango_layout_iter_get_run (iter);
    if (run)
      {
        pango_layout_iter_get_run_extents (iter, NULL, &rect);

        if (pango_rectangle_contains_point (&rect, (int) (px * PANGO_SCALE), (int) (py * PANGO_SCALE)))
          {
            result = run;

            pango_matrix_transform_rectangle (&m, &rect);
            pango_extents_to_pixels (&rect, NULL);

            bounds->x = rect.x;
            bounds->y = rect.y;
            bounds->width = rect.width;
            bounds->height = rect.height;
            break;
          }
      }
  } while (pango_layout_iter_next_run (iter));

  pango_layout_iter_free (iter);

  return result;
}

static const char *
enum_value_nick (GType type,
                 int   value)
{
  GEnumClass *enum_class;
  GEnumValue *enum_value;

  enum_class = g_type_class_ref (type);
  enum_value = g_enum_get_value (enum_class, value);
  g_type_class_unref (enum_class);

  if (enum_value)
    return enum_value->value_nick;
  else
    return "?";
}

static char *
font_name (PangoFont *font)
{
  PangoFontDescription *desc;
  char *name;

  desc = pango_font_describe (font);
  name = pango_font_description_to_string (desc);
  pango_font_description_free (desc);

  return name;
}

static const char *
font_file (PangoFont *font)
{
  FcPattern *pattern;
  char *name;

  pattern = pango_fc_font_get_pattern (PANGO_FC_FONT (font));
  FcPatternGetString (pattern, FC_FILE, 0, (FcChar8 **) &name);

  return name;
}

static char *
flags_string (int flags)
{
  GString *str;

  str = g_string_new ("");

  if (flags & PANGO_ANALYSIS_FLAG_CENTERED_BASELINE)
    g_string_append (str, "BASELINE");
  if (flags & PANGO_ANALYSIS_FLAG_IS_ELLIPSIS)
    {
      if (str->len > 0)
        g_string_append (str, " |\n");
      g_string_append (str, "ELLISPSIS");
    }
  if (flags & PANGO_ANALYSIS_FLAG_NEED_HYPHEN)
    {
      if (str->len > 0)
        g_string_append (str, " |\n");
      g_string_append (str, "HYPHEN");
    }

  if (str->len == 0)
    g_string_append (str, "—");

  return g_string_free (str, FALSE);
}

static void
print_attribute (PangoAttribute *attr,
                 GString        *str)
{
  PangoAttrList *l = pango_attr_list_new ();
  char *s;

  pango_attr_list_insert (l, pango_attribute_copy (attr));
  s = pango_attr_list_to_string (l);
  g_string_append (str, s);
  g_free (s);
  pango_attr_list_unref (l);
}

static char *
print_attributes (GSList *attrs)
{
  GSList *l;
  GString *str;

  str = g_string_new ("");

  for (l = attrs; l; l = l->next)
    {
      PangoAttribute *attr = l->data;
      print_attribute (attr, str);
      g_string_append (str, "\n");
    }

  return g_string_free (str, FALSE);
}

/* FIXME */
struct _PangoItemPrivate
{
  int offset;
  int length;
  int num_chars;
  int char_offset;
  PangoAnalysis analysis;
};

static void
update_run_info (LayoutViewer   *self,
                 PangoLayoutRun *run)
{
  char *str;

  str = g_strdup_printf ("%d – %d", run->item->offset, run->item->offset + run->item->length - 1);
  gtk_label_set_label (self->bytes_label, str);
  g_free (str);

  str = g_strdup_printf ("%d – %d",
                         ((struct _PangoItemPrivate *)run->item)->char_offset,
                         ((struct _PangoItemPrivate *)run->item)->char_offset + run->item->num_chars - 1);
  gtk_label_set_label (self->chars_label, str);
  g_free (str);

  str = g_strndup (pango_layout_get_text (self->layout) + run->item->offset,
                   run->item->length);
  gtk_label_set_label (self->text_label, str);
  g_free (str);

  str = g_strdup_printf ("%d", run->item->analysis.level);
  gtk_label_set_label (self->bidi_label, str);
  g_free (str);

  gtk_label_set_label (self->gravity_label,
                       enum_value_nick (PANGO_TYPE_GRAVITY, run->item->analysis.gravity));


  str = flags_string (run->item->analysis.flags);
  gtk_label_set_label (self->flags_label, str);
  g_free (str);

  str = font_name (run->item->analysis.font);
  gtk_label_set_label (self->font_label, str);
  g_free (str);

  gtk_label_set_label (self->font_file_label, font_file (run->item->analysis.font));

  gtk_label_set_label (self->script_label,
                       enum_value_nick (PANGO_TYPE_SCRIPT, run->item->analysis.script));
  gtk_label_set_label (self->language_label,
                       pango_language_to_string (run->item->analysis.language));

  str = print_attributes (run->item->analysis.extra_attrs);
  gtk_label_set_label (self->attributes_label, str);
  g_free (str);
}

static void
click_cb (GtkGestureClick *gesture,
          int              n_press,
          double           x,
          double           y,
          gpointer         data)
{
  LayoutViewer *self = data;
  PangoLayoutRun *run;
  GdkRectangle rect;

  gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_CLAIMED);

  if (!self->layout)
    return;

  run = find_run_at_pos (self->layout, x, y, &rect);
  if (run)
    {
      update_run_info (self, run);
      self->highlight_run = run;
      gtk_popover_set_pointing_to (self->run_info, &rect);
      self->show_highlight = TRUE;
      gtk_popover_popup (self->run_info);
    }
  else
    {
      gtk_popover_popdown (self->run_info);
    }

  gtk_widget_queue_draw (GTK_WIDGET (self));
}

static void
close_run_info_cb (GtkButton    *button,
                   LayoutViewer *self)
{
  gtk_popover_popdown (self->run_info);
}

static void
run_info_closed_cb (GtkPopover   *popover,
                    LayoutViewer *self)
{
  self->show_highlight = FALSE;
  gtk_widget_queue_draw (GTK_WIDGET (self));
}

/* }}} */
/* {{{ GtkWidget implementation */

static void
layout_viewer_measure (GtkWidget      *widget,
                       GtkOrientation  orientation,
                       int             for_size,
                       int            *minimum,
                       int            *natural,
                       int            *minimum_baseline,
                       int            *natural_baseline)
{
  LayoutViewer *self = (LayoutViewer *) widget;
  int width, height;
  const PangoMatrix *matrix;

  if (!self->layout)
    {
      *minimum = *natural = 0;
      return;
    }

  pango_layout_get_size (self->layout, &width, &height);
  width = MAX (width, pango_layout_get_width (self->layout));

  matrix = pango_context_get_matrix (pango_layout_get_context (self->layout));
  if (matrix)
    {
      PangoRectangle rect;

      rect = (PangoRectangle) { 0, 0, width, height };
      pango_matrix_transform_rectangle (matrix, &rect);
      width = rect.width;
      height = rect.height;
    }

  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    *natural = PANGO_PIXELS_CEIL (width);
  else
    *natural = PANGO_PIXELS_CEIL (height);

  *minimum = *natural;
}

static void
layout_viewer_size_allocate (GtkWidget *widget,
                             int        width,
                             int        height,
                             int        baseline)
{
  LayoutViewer *self = (LayoutViewer *) widget;

  gtk_popover_present (self->run_info);
}

/* }}} */
 /* {{{ GObject boilerplate */

enum {
  PROP_USE_CAIRO = 1,
  PROP_SHOW_BOUNDS,
  PROP_SHOW_TAB_POSITIONS,
  PROP_SHOW_LAYOUT_EXTENTS,
  PROP_SHOW_LINE_EXTENTS,
  PROP_SHOW_RUN_EXTENTS,
  PROP_SHOW_CLUSTER_EXTENTS,
  PROP_SHOW_CHAR_EXTENTS,
  PROP_SHOW_GLYPH_EXTENTS,
  PROP_SHOW_CARET_POSITIONS,
  PROP_SHOW_CARET_SLOPE,
  PROP_SHOW_SENTENCE_BOUNDARIES,
  PROP_SHOW_WORD_BOUNDARIES,
  PROP_SHOW_GRAPHEME_BOUNDARIES,
  PROP_SHOW_BREAK_POSITIONS,
  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL };

G_DEFINE_TYPE (LayoutViewer, layout_viewer, GTK_TYPE_WIDGET);

static void
layout_viewer_init (LayoutViewer *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
layout_viewer_finalize (GObject *object)
{
  LayoutViewer *self = (LayoutViewer *)object;

  g_clear_object (&self->layout);

  G_OBJECT_CLASS (layout_viewer_parent_class)->finalize (object);
}

static void
layout_viewer_dispose (GObject *object)
{
  LayoutViewer *self = (LayoutViewer *)object;

  g_clear_pointer ((GtkWidget **)&self->run_info, gtk_widget_unparent);

  G_OBJECT_CLASS (layout_viewer_parent_class)->dispose (object);
}

static void
layout_viewer_set_property (GObject      *object,
                            guint         property_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  LayoutViewer *self = (LayoutViewer *)object;

  switch (property_id)
    {
    case PROP_USE_CAIRO:
      layout_viewer_set_use_cairo (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_BOUNDS:
      layout_viewer_set_show_bounds (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_TAB_POSITIONS:
      layout_viewer_set_show_tab_positions (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_LAYOUT_EXTENTS:
      layout_viewer_set_show_layout_extents (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_LINE_EXTENTS:
      layout_viewer_set_show_line_extents (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_RUN_EXTENTS:
      layout_viewer_set_show_run_extents (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_CLUSTER_EXTENTS:
      layout_viewer_set_show_cluster_extents (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_CHAR_EXTENTS:
      layout_viewer_set_show_char_extents (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_GLYPH_EXTENTS:
      layout_viewer_set_show_glyph_extents (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_CARET_POSITIONS:
      layout_viewer_set_show_caret_positions (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_CARET_SLOPE:
      layout_viewer_set_show_caret_slope (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_SENTENCE_BOUNDARIES:
      layout_viewer_set_show_sentence_boundaries (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_WORD_BOUNDARIES:
      layout_viewer_set_show_word_boundaries (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_GRAPHEME_BOUNDARIES:
      layout_viewer_set_show_grapheme_boundaries (self, g_value_get_boolean (value));
      break;

    case PROP_SHOW_BREAK_POSITIONS:
      layout_viewer_set_show_break_positions (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
layout_viewer_get_property (GObject    *object,
                            guint       property_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  LayoutViewer *self = (LayoutViewer *)object;

  switch (property_id)
    {
    case PROP_USE_CAIRO:
      g_value_set_boolean (value, self->use_cairo);
      break;

    case PROP_SHOW_BOUNDS:
      g_value_set_boolean (value, self->show_bounds);
      break;

    case PROP_SHOW_TAB_POSITIONS:
      g_value_set_boolean (value, self->show_tab_positions);
      break;

    case PROP_SHOW_LAYOUT_EXTENTS:
      g_value_set_boolean (value, self->show_layout_extents);
      break;

    case PROP_SHOW_LINE_EXTENTS:
      g_value_set_boolean (value, self->show_line_extents);
      break;

    case PROP_SHOW_RUN_EXTENTS:
      g_value_set_boolean (value, self->show_run_extents);
      break;

    case PROP_SHOW_CLUSTER_EXTENTS:
      g_value_set_boolean (value, self->show_cluster_extents);
      break;

    case PROP_SHOW_CHAR_EXTENTS:
      g_value_set_boolean (value, self->show_char_extents);
      break;

    case PROP_SHOW_GLYPH_EXTENTS:
      g_value_set_boolean (value, self->show_glyph_extents);
      break;

    case PROP_SHOW_CARET_POSITIONS:
      g_value_set_boolean (value, self->show_caret_positions);
      break;

    case PROP_SHOW_CARET_SLOPE:
      g_value_set_boolean (value, self->show_caret_slope);
      break;

    case PROP_SHOW_SENTENCE_BOUNDARIES:
      g_value_set_boolean (value, self->show_sentence_boundaries);
      break;

    case PROP_SHOW_WORD_BOUNDARIES:
      g_value_set_boolean (value, self->show_word_boundaries);
      break;

    case PROP_SHOW_GRAPHEME_BOUNDARIES:
      g_value_set_boolean (value, self->show_grapheme_boundaries);
      break;

    case PROP_SHOW_BREAK_POSITIONS:
      g_value_set_boolean (value, self->show_break_positions);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
layout_viewer_class_init (LayoutViewerClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  object_class->finalize = layout_viewer_finalize;
  object_class->dispose = layout_viewer_dispose;
  object_class->set_property = layout_viewer_set_property;
  object_class->get_property = layout_viewer_get_property;

  widget_class->snapshot = layout_viewer_snapshot;
  widget_class->measure = layout_viewer_measure;
  widget_class->size_allocate = layout_viewer_size_allocate;

  properties[PROP_USE_CAIRO] =
    g_param_spec_boolean  ("use-cairo", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_BOUNDS] =
    g_param_spec_boolean  ("show-bounds", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_TAB_POSITIONS] =
    g_param_spec_boolean  ("show-tab-positions", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_LAYOUT_EXTENTS] =
    g_param_spec_boolean  ("show-layout-extents", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_LINE_EXTENTS] =
    g_param_spec_boolean  ("show-line-extents", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_RUN_EXTENTS] =
    g_param_spec_boolean  ("show-run-extents", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_CLUSTER_EXTENTS] =
    g_param_spec_boolean  ("show-cluster-extents", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_CHAR_EXTENTS] =
    g_param_spec_boolean  ("show-char-extents", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_GLYPH_EXTENTS] =
    g_param_spec_boolean  ("show-glyph-extents", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_CARET_POSITIONS] =
    g_param_spec_boolean  ("show-caret-positions", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_CARET_SLOPE] =
    g_param_spec_boolean  ("show-caret-slope", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_SENTENCE_BOUNDARIES] =
    g_param_spec_boolean  ("show-sentence-boundaries", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_WORD_BOUNDARIES] =
    g_param_spec_boolean  ("show-word-boundaries", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_GRAPHEME_BOUNDARIES] =
    g_param_spec_boolean  ("show-grapheme-boundaries", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_SHOW_BREAK_POSITIONS] =
    g_param_spec_boolean  ("show-break-positions", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/pango/LayoutEditor/layout-viewer.ui");

  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, run_info);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, bytes_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, chars_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, text_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, bidi_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, gravity_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, flags_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, font_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, font_file_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, script_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, language_label);
  gtk_widget_class_bind_template_child (widget_class, LayoutViewer, attributes_label);

  gtk_widget_class_bind_template_callback (widget_class, click_cb);
  gtk_widget_class_bind_template_callback (widget_class, close_run_info_cb);
  gtk_widget_class_bind_template_callback (widget_class, run_info_closed_cb);

  gtk_widget_class_set_css_name (widget_class, "layout-viewer");
}

  /* }}} */
/* {{{ Public API */

LayoutViewer *
layout_viewer_new (void)
{
  return g_object_new (layout_viewer_get_type (), NULL);
}

void
layout_viewer_set_layout (LayoutViewer *self,
                          PangoLayout  *layout)
{
  if (self->layout)
    g_object_unref (self->layout);
  self->layout = layout;
  if (self->layout)
    g_object_ref (self->layout);
  update_from_layout (self);
}

gboolean
layout_viewer_get_use_cairo (LayoutViewer *self)
{
  return self->use_cairo;
}

void
layout_viewer_set_use_cairo (LayoutViewer *self,
                             gboolean      use_cairo)
{
  if (self->use_cairo == use_cairo)
    return;

  self->use_cairo = use_cairo;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_USE_CAIRO]);
}

gboolean
layout_viewer_get_show_bounds (LayoutViewer *self)
{
  return self->show_bounds;
}

void
layout_viewer_set_show_bounds (LayoutViewer *self,
                               gboolean      show_bounds)
{
  if (self->show_bounds == show_bounds)
    return;

  self->show_bounds = show_bounds;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_BOUNDS]);
}

gboolean
layout_viewer_get_show_tab_positions (LayoutViewer *self)
{
  return self->show_tab_positions;
}

void
layout_viewer_set_show_tab_positions (LayoutViewer *self,
                                      gboolean      show_tab_positions)
{
  if (self->show_tab_positions == show_tab_positions)
    return;

  self->show_tab_positions = show_tab_positions;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_TAB_POSITIONS]);
}

gboolean
layout_viewer_get_show_layout_extents (LayoutViewer *self)
{
  return self->show_layout_extents;
}

void
layout_viewer_set_show_layout_extents (LayoutViewer *self,
                                       gboolean      show_layout_extents)
{
  if (self->show_layout_extents == show_layout_extents)
    return;

  self->show_layout_extents = show_layout_extents;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_RUN_EXTENTS]);
}

gboolean
layout_viewer_get_show_line_extents (LayoutViewer *self)
{
  return self->show_line_extents;
}

void
layout_viewer_set_show_line_extents (LayoutViewer *self,
                                     gboolean      show_line_extents)
{
  if (self->show_line_extents == show_line_extents)
    return;

  self->show_line_extents = show_line_extents;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_LINE_EXTENTS]);
}

gboolean
layout_viewer_get_show_run_extents (LayoutViewer *self)
{
  return self->show_run_extents;
}

void
layout_viewer_set_show_run_extents (LayoutViewer *self,
                                    gboolean      show_run_extents)
{
  if (self->show_run_extents == show_run_extents)
    return;

  self->show_run_extents = show_run_extents;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_RUN_EXTENTS]);
}

gboolean
layout_viewer_get_show_cluster_extents (LayoutViewer *self)
{
  return self->show_cluster_extents;
}

void
layout_viewer_set_show_cluster_extents (LayoutViewer *self,
                                        gboolean      show_cluster_extents)
{
  if (self->show_cluster_extents == show_cluster_extents)
    return;

  self->show_cluster_extents = show_cluster_extents;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_CLUSTER_EXTENTS]);
}

gboolean
layout_viewer_get_show_char_extents (LayoutViewer *self)
{
  return self->show_char_extents;
}

void
layout_viewer_set_show_char_extents (LayoutViewer *self,
                                     gboolean      show_char_extents)
{
  if (self->show_char_extents == show_char_extents)
    return;

  self->show_char_extents = show_char_extents;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_CHAR_EXTENTS]);
}

gboolean
layout_viewer_get_show_glyph_extents (LayoutViewer *self)
{
  return self->show_glyph_extents;
}

void
layout_viewer_set_show_glyph_extents (LayoutViewer *self,
                                      gboolean      show_glyph_extents)
{
  if (self->show_glyph_extents == show_glyph_extents)
    return;

  self->show_glyph_extents = show_glyph_extents;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_GLYPH_EXTENTS]);
}

gboolean
layout_viewer_get_show_caret_positions (LayoutViewer *self)
{
  return self->show_caret_positions;
}

void
layout_viewer_set_show_caret_positions (LayoutViewer *self,
                                        gboolean      show_caret_positions)
{
  if (self->show_caret_positions == show_caret_positions)
    return;

  self->show_caret_positions = show_caret_positions;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_CARET_POSITIONS]);
}

gboolean
layout_viewer_get_show_caret_slope (LayoutViewer *self)
{
  return self->show_caret_slope;
}

void
layout_viewer_set_show_caret_slope (LayoutViewer *self,
                                    gboolean      show_caret_slope)
{
  if (self->show_caret_slope == show_caret_slope)
    return;

  self->show_caret_slope = show_caret_slope;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_CARET_SLOPE]);
}

gboolean
layout_viewer_get_show_sentence_boundaries (LayoutViewer *self)
{
  return self->show_sentence_boundaries;
}

void
layout_viewer_set_show_sentence_boundaries (LayoutViewer *self,
                                            gboolean      show_sentence_boundaries)
{
  if (self->show_sentence_boundaries == show_sentence_boundaries)
    return;

  self->show_sentence_boundaries = show_sentence_boundaries;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_SENTENCE_BOUNDARIES]);
}

gboolean
layout_viewer_get_show_word_boundaries (LayoutViewer *self)
{
  return self->show_word_boundaries;
}

void
layout_viewer_set_show_word_boundaries (LayoutViewer *self,
                                        gboolean      show_word_boundaries)
{
  if (self->show_word_boundaries == show_word_boundaries)
    return;

  self->show_word_boundaries = show_word_boundaries;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_WORD_BOUNDARIES]);
}

gboolean
layout_viewer_get_show_grapheme_boundaries (LayoutViewer *self)
{
  return self->show_grapheme_boundaries;
}

void
layout_viewer_set_show_grapheme_boundaries (LayoutViewer *self,
                                            gboolean      show_grapheme_boundaries)
{
  if (self->show_grapheme_boundaries == show_grapheme_boundaries)
    return;

  self->show_grapheme_boundaries = show_grapheme_boundaries;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_GRAPHEME_BOUNDARIES]);
}

gboolean
layout_viewer_get_show_break_positions (LayoutViewer *self)
{
  return self->show_break_positions;
}

void
layout_viewer_set_show_break_positions (LayoutViewer *self,
                                        gboolean      show_break_positions)
{
  if (self->show_break_positions == show_break_positions)
    return;

  self->show_break_positions = show_break_positions;

  gtk_widget_queue_draw (GTK_WIDGET (self));
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_BREAK_POSITIONS]);
}

/* }}} */

/* vim:set foldmethod=marker expandtab: */
