/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#pragma once

#include <gtk/gtk.h>

G_DECLARE_FINAL_TYPE (LayoutViewer, layout_viewer, LAYOUT, VIEWER, GtkWidget)

LayoutViewer *          layout_viewer_new                  (void);

void                    layout_viewer_set_layout           (LayoutViewer *self,
                                                            PangoLayout  *layout);

void                    layout_viewer_set_use_cairo        (LayoutViewer *self,
                                                            gboolean      use_cairo);
gboolean                layout_viewer_get_use_cairo        (LayoutViewer *self);

void                    layout_viewer_set_show_bounds      (LayoutViewer *self,
                                                            gboolean      show_bounds);
gboolean                layout_viewer_get_show_bounds      (LayoutViewer *self);

void                    layout_viewer_set_show_tab_positions (LayoutViewer *self,
                                                              gboolean      show_tab_positions);
gboolean                layout_viewer_get_show_tab_positions (LayoutViewer *self);

void                    layout_viewer_set_show_layout_extents (LayoutViewer *self,
                                                               gboolean      show_layout_extents);
gboolean                layout_viewer_get_show_layout_extents (LayoutViewer *self);

void                    layout_viewer_set_show_line_extents (LayoutViewer *self,
                                                             gboolean      show_line_extents);
gboolean                layout_viewer_get_show_line_extents (LayoutViewer *self);

void                    layout_viewer_set_show_run_extents (LayoutViewer *self,
                                                            gboolean      show_run_extents);
gboolean                layout_viewer_get_show_run_extents (LayoutViewer *self);

void                    layout_viewer_set_show_cluster_extents (LayoutViewer *self,
                                                                gboolean      show_cluster_extents);
gboolean                layout_viewer_get_show_cluster_extents (LayoutViewer *self);

void                    layout_viewer_set_show_char_extents (LayoutViewer *self,
                                                             gboolean      show_char_extents);
gboolean                layout_viewer_get_show_char_extents (LayoutViewer *self);

void                    layout_viewer_set_show_glyph_extents (LayoutViewer *self,
                                                              gboolean      show_glyph_extents);
gboolean                layout_viewer_get_show_glyph_extents (LayoutViewer *self);

void                    layout_viewer_set_show_caret_positions (LayoutViewer *self,
                                                                gboolean      show_caret_positions);
gboolean                layout_viewer_get_show_caret_positions (LayoutViewer *self);

void                    layout_viewer_set_show_caret_slope (LayoutViewer *self,
                                                            gboolean      show_caret_slope);
gboolean                layout_viewer_get_show_caret_slope (LayoutViewer *self);

void                    layout_viewer_set_show_sentence_boundaries (LayoutViewer *self,
                                                                    gboolean      show_sentence_boundaries);
gboolean                layout_viewer_get_show_sentence_boundaries (LayoutViewer *self);

void                    layout_viewer_set_show_word_boundaries (LayoutViewer *self,
                                                                gboolean      show_word_boundaries);
gboolean                layout_viewer_get_show_word_boundaries (LayoutViewer *self);

void                    layout_viewer_set_show_grapheme_boundaries (LayoutViewer *self,
                                                                    gboolean      show_grapheme_boundaries);
gboolean                layout_viewer_get_show_grapheme_boundaries (LayoutViewer *self);

void                    layout_viewer_set_show_break_positions (LayoutViewer *self,
                                                                gboolean      show_break_positions);
gboolean                layout_viewer_get_show_break_positions (LayoutViewer *self);
