/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#include "config.h"

#include <math.h>

#include "context-editor.h"

struct _ContextEditor
{
  GtkWidget parent;

  GtkWidget *box;
  GtkEntry *language;
  GtkDropDown *base_gravity;
  GtkToggleButton *hint_natural;
  GtkToggleButton *hint_strong;
  GtkToggleButton *hint_line;
  GtkDropDown *base_direction;
  GtkSpinButton *angle;
  GtkSpinButton *scale_x;
  GtkSpinButton *scale_y;
  GtkSpinButton *skew_x;
  GtkSpinButton *skew_y;
  GtkSwitch *round_positions;

  PangoContext *context;
};

struct _ContextEditorClass
{
  GtkWidgetClass parent_class;
};

/* {{{ Editor implementation */

static int
pango_direction_to_model (PangoDirection direction)
{
  switch (direction)
    {
    case PANGO_DIRECTION_TTB_LTR:
    case PANGO_DIRECTION_LTR:
      return 0;
    case PANGO_DIRECTION_TTB_RTL:
    case PANGO_DIRECTION_RTL:
      return 1;
    case PANGO_DIRECTION_WEAK_LTR:
      return 2;
    case PANGO_DIRECTION_WEAK_RTL:
      return 3;
    case PANGO_DIRECTION_NEUTRAL:
      return 4;
    default:
      g_assert_not_reached ();
    }
}

static PangoDirection
pango_direction_from_model (int pos)
{
  switch (pos)
    {
    case 0:
      return PANGO_DIRECTION_LTR;
    case 1:
      return PANGO_DIRECTION_RTL;
    case 2:
      return PANGO_DIRECTION_WEAK_LTR;
    case 3:
      return PANGO_DIRECTION_WEAK_RTL;
    case 4:
      return PANGO_DIRECTION_NEUTRAL;
    default:
      g_assert_not_reached ();
    }
}


#define DEG_TO_RAD(x) ((x) / 180.f * G_PI)
#define RAD_TO_DEG(x) ((x) * 180.f / G_PI)

static void
matrix_to_2d_components (const PangoMatrix *matrix,
                         float        *out_skew_x,
                         float        *out_skew_y,
                         float        *out_scale_x,
                         float        *out_scale_y,
                         float        *out_angle,
                         float        *out_dx,
                         float        *out_dy)

{
  PangoMatrix m = matrix ? *matrix : (PangoMatrix) PANGO_MATRIX_INIT;
  float a = m.xx;
  float b = m.xy;
  float c = m.yx;
  float d = m.yy;
  float e = m.x0;
  float f = m.y0;

  *out_dx = e;
  *out_dy = f;

#define sign(f) ((f) < 0 ? -1 : 1)

  if (a != 0 || b != 0)
    {
      float det = a * d - b * c;
      float r = sqrtf (a*a + b*b);

      *out_angle = RAD_TO_DEG (sign (b) * acosf (a / r));
      *out_scale_x = r;
      *out_scale_y = det / r;
      *out_skew_x = RAD_TO_DEG (atanf ((a*c + b*d) / (r*r)));
      *out_skew_y = 0;
    }
  else if (c != 0 || d != 0)
    {
      float det = a * d - b * c;
      float s = sqrtf (c*c + d*d);

      *out_angle = RAD_TO_DEG (G_PI/2 - sign (d) * acosf (-c / s));
      *out_scale_x = det / s;
      *out_scale_y = s;
      *out_skew_x = 0;
      *out_skew_y = RAD_TO_DEG (atanf ((a*c + b*d) / (s*s)));
    }
  else
    {
      *out_angle = 0;
      *out_scale_x = 1;
      *out_scale_y = 1;
      *out_skew_x = 0;
      *out_skew_y = 0;
    }
}

static void
pango_matrix_skew (PangoMatrix *matrix,
                   float        skew_x,
                   float        skew_y)
{
  float tx, ty;

  tx = tanf (DEG_TO_RAD (skew_x));
  ty = tanf (DEG_TO_RAD (skew_y));
  pango_matrix_concat (matrix, &(PangoMatrix) { 1 + tx*ty, ty, tx, 1, 0, 0 });
}

static void
update_editor_from_context (ContextEditor *self,
                            PangoContext  *context)
{
  float skew_x, skew_y, scale_x, scale_y, angle, dx, dy;

  if (!context)
    {
      gtk_widget_remove_css_class (GTK_WIDGET (self->language), "error");
      gtk_editable_set_text (GTK_EDITABLE (self->language),
                             pango_language_to_string (pango_language_get_default ()));
      gtk_drop_down_set_selected (self->base_gravity, PANGO_GRAVITY_AUTO);
      gtk_toggle_button_set_active (self->hint_natural, TRUE);
      gtk_drop_down_set_selected (self->base_direction, PANGO_DIRECTION_NEUTRAL);
      gtk_switch_set_active (self->round_positions, TRUE);
      gtk_spin_button_set_value (self->angle, 0);
      gtk_spin_button_set_value (self->scale_x, 1);
      gtk_spin_button_set_value (self->scale_y, 1);
      gtk_spin_button_set_value (self->skew_x, 0);
      gtk_spin_button_set_value (self->skew_y, 0);

      return;
    }

  gtk_widget_remove_css_class (GTK_WIDGET (self->language), "error");
  if (pango_context_get_language (context))
    gtk_editable_set_text (GTK_EDITABLE (self->language),
                           pango_language_to_string (pango_context_get_language (context)));

  gtk_drop_down_set_selected (self->base_gravity, pango_context_get_base_gravity (context));

  switch (pango_context_get_gravity_hint (context))
    {
    case PANGO_GRAVITY_HINT_NATURAL:
      gtk_toggle_button_set_active (self->hint_natural, TRUE);
      break;
    case PANGO_GRAVITY_HINT_STRONG:
      gtk_toggle_button_set_active (self->hint_strong, TRUE);
      break;
    case PANGO_GRAVITY_HINT_LINE:
      gtk_toggle_button_set_active (self->hint_line, TRUE);
      break;
    default:
      g_assert_not_reached ();
    }

  gtk_drop_down_set_selected (self->base_direction,
                              pango_direction_to_model (pango_context_get_base_dir (context)));

  matrix_to_2d_components (pango_context_get_matrix (context),
                           &skew_x, &skew_y,
                           &scale_x, &scale_y,
                           &angle,
                           &dx, &dy);
  gtk_spin_button_set_value (self->skew_x, skew_x);
  gtk_spin_button_set_value (self->skew_y, skew_y);
  gtk_spin_button_set_value (self->scale_x, scale_x);
  gtk_spin_button_set_value (self->scale_y, scale_y);
  gtk_spin_button_set_value (self->angle, angle);

  gtk_switch_set_active (self->round_positions,
                         pango_context_get_round_glyph_positions (context));
}

static void
language_changed (GtkEditable   *editable,
                  ContextEditor *self)
{
  const char *text;
  PangoLanguage *language;

  if (!self->context)
    return;

  text = gtk_editable_get_text (editable);
  language = pango_language_from_string (text);

  if (language)
    {
      gtk_widget_remove_css_class (GTK_WIDGET (editable), "error");
      pango_context_set_language (self->context, language);
    }
  else
    {
      gtk_widget_add_css_class (GTK_WIDGET (editable), "error");
    }
}
static void
base_gravity_changed (GtkDropDown   *dropdown,
                      GParamSpec    *pspec,
                      ContextEditor *self)
{
  if (!self->context)
    return;

  pango_context_set_base_gravity (self->context, gtk_drop_down_get_selected (dropdown));
}

static void
gravity_hint_changed (GtkToggleButton *button,
                      GParamSpec      *pspec,
                      ContextEditor   *self)
{
  PangoGravityHint gravity_hint;

  if (!self->context)
    return;

  if (!gtk_toggle_button_get_active (button))
    return;

  if (button == self->hint_natural)
    gravity_hint = PANGO_GRAVITY_HINT_NATURAL;
  else if (button == self->hint_strong)
    gravity_hint = PANGO_GRAVITY_HINT_STRONG;
  else if (button == self->hint_line)
    gravity_hint = PANGO_GRAVITY_HINT_LINE;
  else
    g_assert_not_reached ();

  pango_context_set_gravity_hint (self->context, gravity_hint);
}

static void
direction_changed (GtkDropDown   *dropdown,
                   GParamSpec    *pspec,
                   ContextEditor *self)
{
  if (!self->context)
    return;

  pango_context_set_base_dir (self->context,
                              pango_direction_from_model (gtk_drop_down_get_selected (dropdown)));
}

static void
matrix_changed (GtkSpinButton *button,
                ContextEditor *self)
{
  PangoMatrix matrix = PANGO_MATRIX_INIT;

  if (!self->context)
    return;

  pango_matrix_rotate (&matrix, gtk_spin_button_get_value (self->angle));
  pango_matrix_scale (&matrix,
                      gtk_spin_button_get_value (self->scale_x),
                      gtk_spin_button_get_value (self->scale_y));
  pango_matrix_skew (&matrix,
                     gtk_spin_button_get_value (self->skew_x),
                     gtk_spin_button_get_value (self->skew_y));

  pango_context_set_matrix (self->context, &matrix);
}

static void
round_positions_changed (GtkSwitch     *widget,
                         GParamSpec    *pspec,
                         ContextEditor *self)
{
  if (!self->context)
    return;

  pango_context_set_round_glyph_positions (self->context,
                                           gtk_switch_get_active (widget));
}

/*  }}} */
/* {{{ GObject boilerplate */

G_DEFINE_TYPE (ContextEditor, context_editor, GTK_TYPE_WIDGET);

static void
context_editor_init (ContextEditor *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
context_editor_finalize (GObject *object)
{
  ContextEditor *self = (ContextEditor *)object;

  g_clear_object (&self->context);

  G_OBJECT_CLASS (context_editor_parent_class)->finalize (object);
}

static void
context_editor_dispose (GObject *object)
{
  ContextEditor *self = (ContextEditor *)object;

  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (context_editor_parent_class)->dispose (object);
}

static void
context_editor_class_init (ContextEditorClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  object_class->finalize = context_editor_finalize;
  object_class->dispose = context_editor_dispose;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/pango/LayoutEditor/context-editor.ui");

  gtk_widget_class_bind_template_child (widget_class, ContextEditor, box);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, language);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, base_gravity);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, hint_natural);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, hint_strong);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, hint_line);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, base_direction);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, angle);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, scale_x);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, scale_y);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, skew_x);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, skew_y);
  gtk_widget_class_bind_template_child (widget_class, ContextEditor, round_positions);

  gtk_widget_class_bind_template_callback (widget_class, language_changed);
  gtk_widget_class_bind_template_callback (widget_class, direction_changed);
  gtk_widget_class_bind_template_callback (widget_class, base_gravity_changed);
  gtk_widget_class_bind_template_callback (widget_class, gravity_hint_changed);
  gtk_widget_class_bind_template_callback (widget_class, matrix_changed);
  gtk_widget_class_bind_template_callback (widget_class, round_positions_changed);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);

  gtk_widget_class_set_css_name (widget_class, "context-editor");
}

/* }}} */
/* {{{ Public API */

ContextEditor *
context_editor_new (void)
{
  return g_object_new (context_editor_get_type (), NULL);
}

void
context_editor_set_context (ContextEditor *self,
                            PangoContext  *context)
{
  g_clear_object (&self->context);
  update_editor_from_context (self, context);
  self->context = context;
  if (self->context)
    g_object_ref (self->context);
}

/* }}} */

/* vim:set foldmethod=marker expandtab: */
