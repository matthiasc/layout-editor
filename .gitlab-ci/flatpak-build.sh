#!/bin/bash

set -e

appid=org.pango.LayoutEditor
builddir=flatpak_app
repodir=repo

flatpak-builder \
        --user --disable-rofiles-fuse \
        --repo=${repodir} \
        ${builddir} \
        build-aux/flatpak/${appid}.json

flatpak build-bundle \
        ${repodir} \
        ${appid}-dev.flatpak \
        --runtime-repo=https://nightly.gnome.org/gnome-nightly.flatpakrepo \
        ${appid}
