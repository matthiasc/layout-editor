/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#include "config.h"

#include "layout-editor-application.h"

#include "layout-editor-window.h"

struct _LayoutEditorApplication
{
  GtkApplication parent;
};

struct _LayoutEditorApplicationClass
{
  GtkApplicationClass parent_class;
};

G_DEFINE_TYPE (LayoutEditorApplication, layout_editor_application, GTK_TYPE_APPLICATION);

static void
layout_editor_application_init (LayoutEditorApplication *app)
{
}

static void
activate_about (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       user_data)
{
  GtkApplication *app = user_data;
  GString *s;
  char *os_name;
  char *os_version;
  GtkWidget *dialog;

  os_name = g_get_os_info (G_OS_INFO_KEY_NAME);
  os_version = g_get_os_info (G_OS_INFO_KEY_VERSION_ID);
  s = g_string_new ("");
  if (os_name && os_version)
    g_string_append_printf (s, "OS\t%s %s\n\n", os_name, os_version);

  g_string_append (s, "System libraries\n");
  g_string_append_printf (s, "\tGLib\t%d.%d.%d\n",
                          glib_major_version,
                          glib_minor_version,
                          glib_micro_version);
  g_string_append_printf (s, "\tPango\t%s\n",
                          pango_version_string ());
  g_string_append_printf (s, "\tGTK \t%d.%d.%d\n",
                          gtk_get_major_version (),
                          gtk_get_minor_version (),
                          gtk_get_micro_version ());

  dialog = g_object_new (GTK_TYPE_ABOUT_DIALOG,
                         "transient-for", gtk_application_get_active_window (app),
                         "modal", TRUE,
                         "program-name",  "Pango Layout Editor",
                         "version", PACKAGE_VERSION,
                         "copyright", "© 2021 Matthias Clasen",
                         "license-type", GTK_LICENSE_LGPL_2_1,
                         "website", "http://www.gtk.org",
                         "comments", "Program to explore Pango",
                         "authors", (const char *[]){ "Matthias Clasen", NULL},
                         "logo-icon-name", "org.pango.LayoutEditor.Devel",
                         "title", "About Pango Layout Editor",
                         "system-information", s->str,
                         NULL);

  gtk_about_dialog_add_credit_section (GTK_ABOUT_DIALOG (dialog),
                                       "Artwork by", (const char *[]) { "Jakub Steiner", NULL });

  gtk_window_present (GTK_WINDOW (dialog));

  g_string_free (s, TRUE);
  g_free (os_name);
  g_free (os_version);
}

static void
activate_quit (GSimpleAction *action,
               GVariant      *parameter,
               gpointer       data)
{
  g_application_quit (G_APPLICATION (data));
}

static void
activate_inspector (GSimpleAction *action,
                    GVariant      *parameter,
                    gpointer       user_data)
{
  gtk_window_set_interactive_debugging (TRUE);
}

static void
activate_help (GSimpleAction *action,
               GVariant      *parameter,
               gpointer       user_data)
{
  GtkBuilder *builder;
  GtkWidget *window;
  GtkTextBuffer *buffer;
  GBytes *bytes;
  const char *text;
  gsize len;
  GtkTextIter start;

  builder = gtk_builder_new ();
  gtk_builder_add_from_resource (builder, "/org/pango/LayoutEditor/help-window.ui", NULL);
  window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
  buffer = GTK_TEXT_BUFFER (gtk_builder_get_object (builder, "buffer"));

  bytes = g_resources_lookup_data ("/org/pango/LayoutEditor/help.txt",
                                   G_RESOURCE_LOOKUP_FLAGS_NONE,
                                   NULL);
  text = g_bytes_get_data (bytes, &len);
  gtk_text_buffer_get_start_iter (buffer, &start);
  gtk_text_buffer_insert_markup (buffer, &start, text, len);
  g_bytes_unref (bytes);

  gtk_window_present (GTK_WINDOW (window));
  g_object_unref (builder);
}

static GActionEntry app_entries[] =
{
  { "about", activate_about, NULL, NULL, NULL },
  { "quit", activate_quit, NULL, NULL, NULL },
  { "inspector", activate_inspector, NULL, NULL, NULL },
  { "help", activate_help, NULL, NULL, NULL },
};

static void
layout_editor_application_startup (GApplication *app)
{
  GtkCssProvider *provider;
  const char *help_accels[2] = { "F1", NULL };
  const char *quit_accels[2] = { "<Ctrl>Q", NULL };
  const char *open_accels[2] = { "<Ctrl>O", NULL };

  G_APPLICATION_CLASS (layout_editor_application_parent_class)->startup (app);

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries, G_N_ELEMENTS (app_entries),
                                   app);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.help", help_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "app.quit", quit_accels);
  gtk_application_set_accels_for_action (GTK_APPLICATION (app), "win.open", open_accels);

  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (provider, "/org/pango/LayoutEditor/layout-editor.css");
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (provider),
                                              GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

static void
layout_editor_application_activate (GApplication *app)
{
  LayoutEditorWindow *win;

  win = layout_editor_window_new (LAYOUT_EDITOR_APPLICATION (app));
  gtk_window_present (GTK_WINDOW (win));
}

static int
layout_editor_application_command_line (GApplication            *app,
                                        GApplicationCommandLine *cmdline)
{
  GVariantDict *options;
  const char *path = NULL;
  const char **files = NULL;
  GFile *fonts = NULL;

  options = g_application_command_line_get_options_dict (cmdline);
  if (g_variant_dict_lookup (options, "fonts", "^&ay", &path))
    fonts = g_file_new_for_commandline_arg (path);

  if (g_variant_dict_lookup (options, G_OPTION_REMAINING, "^a&ay", &files))
    {
      for (int i = 0; files[i]; i++)
        {
          LayoutEditorWindow *win;
          GFile *file;

          file = g_file_new_for_commandline_arg (files[i]);

          win = layout_editor_window_new (LAYOUT_EDITOR_APPLICATION (app));
          layout_editor_window_set_fonts (win, fonts);
          layout_editor_window_load (win, file);
          gtk_window_present (GTK_WINDOW (win));

          g_object_unref (file);
        }
    }
  else
    {
      LayoutEditorWindow *win;

      win = layout_editor_window_new (LAYOUT_EDITOR_APPLICATION (app));
      layout_editor_window_set_fonts (win, fonts);
      gtk_window_present (GTK_WINDOW (win));
    }

  g_clear_object (&fonts);

  return 0;
}

static void
layout_editor_application_class_init (LayoutEditorApplicationClass *class)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (class);

  application_class->startup = layout_editor_application_startup;
  application_class->activate = layout_editor_application_activate;
  application_class->command_line = layout_editor_application_command_line;
}

LayoutEditorApplication *
layout_editor_application_new (void)
{
  GApplication *app;

  app = g_object_new (layout_editor_application_get_type (),
                      "application-id", "org.pango.LayoutEditor",
                      "flags", G_APPLICATION_HANDLES_COMMAND_LINE,
                      NULL);

  g_application_add_main_option (app, "fonts", 0, 0, G_OPTION_ARG_FILENAME, "Fonts to use", "DIR");
  g_application_add_main_option (app, G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_FILENAME_ARRAY, "Files", NULL);

  return (LayoutEditorApplication *) app;
}
