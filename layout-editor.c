/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#include "config.h"

#include "layout-editor.h"
#include "size-editor.h"

struct _LayoutEditor
{
  GtkWidget parent;

  GtkWidget *box;
  GtkTextView *comment_editor;
  GtkTextView *text_editor;
  GtkTextView *attrs_editor;
  GtkTextView *tabs_editor;
  GtkFontButton *font_editor;
  SizeEditor *indent_editor;
  SizeEditor *width_editor;
  SizeEditor *height_editor;
  GtkSwitch *justify_editor;
  GtkSwitch *justify_last_line_editor;
  GtkSwitch *single_paragraph_editor;
  GtkSwitch *auto_dir_editor;
  GtkDropDown *alignment_editor;
  GtkDropDown *wrap_editor;
  GtkDropDown *ellipsize_editor;
  SizeEditor *spacing_editor;
  GtkSpinButton *line_spacing_editor;

  GtkTextView *json_editor;

  GtkStack *stack;
  gboolean json_mode;
  char *json_error;
  gsize json_error_start;
  gsize json_error_end;

  PangoLayout *layout;

  gboolean skip_json_update;
};

struct _LayoutEditorClass
{
  GtkWidgetClass parent_class;
};

/* {{{ Editor implementation */

static void text_changed (GtkTextBuffer *buffer,
                          gpointer       data);

static void
update_text_tags (LayoutEditor  *self,
                  GtkTextBuffer *buffer)
{
  GtkTextIter start, end;

  gtk_text_buffer_get_bounds (buffer, &start, &end);

  /* Ugh. Having global tags in GtkTextBuffer is somewhat inconvenient. */
  g_signal_handlers_block_by_func (buffer, text_changed, self);
  gtk_text_buffer_remove_all_tags (buffer, &start, &end);
  gtk_text_buffer_apply_tag_by_name (buffer, "show-ignorables", &start, &end);
  g_signal_handlers_unblock_by_func (buffer, text_changed, self);
}

static void
update_editor_from_layout (LayoutEditor *self,
                           PangoLayout  *layout)
{
  GtkTextBuffer *comment_buffer;
  GtkTextBuffer *text_buffer;
  GtkTextBuffer *attrs_buffer;
  GtkTextBuffer *tabs_buffer;
  PangoAttrList *attrs;
  PangoTabArray *tabs;
  const PangoFontDescription *desc;
  char *str;

  comment_buffer = gtk_text_view_get_buffer (self->comment_editor);
  text_buffer = gtk_text_view_get_buffer (self->text_editor);
  attrs_buffer = gtk_text_view_get_buffer (self->attrs_editor);
  tabs_buffer = gtk_text_view_get_buffer (self->tabs_editor);

  if (!layout)
    {
      gtk_text_buffer_set_text (comment_buffer, "", 0);
      gtk_text_buffer_set_text (text_buffer, "", 0);
      gtk_text_buffer_set_text (attrs_buffer, "", 0);
      gtk_text_buffer_set_text (tabs_buffer, "", 0);
      size_editor_set_value (self->indent_editor, 0);
      size_editor_set_value (self->width_editor, -1);
      size_editor_set_value (self->height_editor, -1);
      gtk_switch_set_active (self->justify_editor, FALSE);
      gtk_switch_set_active (self->justify_last_line_editor, FALSE);
      gtk_switch_set_active (self->single_paragraph_editor, FALSE);
      gtk_switch_set_active (self->auto_dir_editor, TRUE);
      gtk_drop_down_set_selected (self->alignment_editor, 0);
      gtk_drop_down_set_selected (self->wrap_editor, 0);
      gtk_drop_down_set_selected (self->ellipsize_editor, 0);
      size_editor_set_value (self->spacing_editor, 0);
      gtk_spin_button_set_value (self->line_spacing_editor, 0);
      gtk_font_chooser_set_font_map (GTK_FONT_CHOOSER (self->font_editor), NULL);

      return;
    }

  str = (char *) g_object_get_data (G_OBJECT (layout), "comment");
  gtk_text_buffer_set_text (comment_buffer, str ? str : "", -1);
  gtk_text_buffer_set_text (text_buffer, pango_layout_get_text (layout), -1);
  update_text_tags (self, text_buffer);

  attrs = pango_layout_get_attributes (layout);
  if (attrs)
    {
      str = pango_attr_list_to_string (attrs);
      gtk_text_buffer_set_text (attrs_buffer, str, -1);
      g_free (str);
    }
  else
    gtk_text_buffer_set_text (attrs_buffer, "", 0);

  tabs = pango_layout_get_tabs (layout);
  if (tabs)
    {
      str = pango_tab_array_to_string (tabs);
      gtk_text_buffer_set_text (tabs_buffer, str, -1);
      g_free (str);
      pango_tab_array_free (tabs);
    }
  else
    gtk_text_buffer_set_text (tabs_buffer, "", -1);

  desc = pango_layout_get_font_description (layout);
  if (desc)
    gtk_font_chooser_set_font_desc (GTK_FONT_CHOOSER (self->font_editor), desc);

  size_editor_set_value (self->indent_editor, pango_layout_get_indent (layout));
  size_editor_set_value (self->width_editor, pango_layout_get_width (layout));
  size_editor_set_value (self->height_editor, pango_layout_get_height (layout));

  gtk_switch_set_active (self->justify_editor, pango_layout_get_justify (layout));
  gtk_switch_set_active (self->justify_last_line_editor, pango_layout_get_justify_last_line (layout));
  gtk_switch_set_active (self->single_paragraph_editor, pango_layout_get_single_paragraph_mode (layout));
  gtk_switch_set_active (self->auto_dir_editor, pango_layout_get_auto_dir (layout));

  gtk_drop_down_set_selected (self->alignment_editor, pango_layout_get_alignment (layout));
  gtk_drop_down_set_selected (self->wrap_editor, pango_layout_get_wrap (layout));
  gtk_drop_down_set_selected (self->ellipsize_editor, pango_layout_get_ellipsize (layout));

  size_editor_set_value (self->spacing_editor, pango_layout_get_spacing (layout));
  gtk_spin_button_set_value (self->line_spacing_editor, pango_layout_get_line_spacing (layout));

  gtk_font_chooser_set_font_map (GTK_FONT_CHOOSER (self->font_editor),
                                 pango_context_get_font_map (pango_layout_get_context (layout)));
}

static void
update_json_from_layout (LayoutEditor *self,
                         PangoLayout  *layout)
{
  GtkTextBuffer *json_buffer;
  GBytes *bytes;
  PangoLayout *saved;

  if (self->skip_json_update)
    return;

  saved = self->layout;
  self->layout = NULL;

  json_buffer = gtk_text_view_get_buffer (self->json_editor);

  if (!layout)
    {
      gtk_text_buffer_set_text (json_buffer, "", 0);

      self->layout = saved;
      return;
    }

  bytes = pango_layout_serialize (layout, PANGO_LAYOUT_SERIALIZE_DEFAULT);
  gtk_text_buffer_set_text (json_buffer, (const char *)g_bytes_get_data (bytes, NULL),
                                         (int)g_bytes_get_size (bytes));
  g_bytes_unref (bytes);

  self->layout = saved;
}

static void
text_changed (GtkTextBuffer *buffer,
              gpointer       data)
{
  LayoutEditor *self = data;
  GtkTextIter start, end;
  char *text;

  if (!self->layout)
    return;

  update_text_tags (self, buffer);

  gtk_text_buffer_get_bounds (buffer, &start, &end);
  text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);

  pango_layout_set_text (self->layout, text, -1);
  g_free (text);

  update_json_from_layout (self, self->layout);
}

static void
attrs_changed (GtkTextBuffer *buffer,
               gpointer       data)
{
  LayoutEditor *self = data;
  GtkTextIter start, end;
  char *text;
  PangoAttrList *attrs;

  if (!self->layout)
    return;

  gtk_text_buffer_get_bounds (buffer, &start, &end);
  text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
  attrs = pango_attr_list_from_string (text);
  pango_layout_set_attributes (self->layout, attrs);
  pango_attr_list_unref (attrs);
  g_free (text);

  update_json_from_layout (self, self->layout);
}

static void
comment_changed (GtkTextBuffer *buffer,
                 gpointer       data)
{
  LayoutEditor *self = data;
  GtkTextIter start, end;
  char *text;

  if (!self->layout)
    return;

  gtk_text_buffer_get_bounds (buffer, &start, &end);
  text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
  g_object_set_data_full (G_OBJECT (self->layout), "comment", text, g_free);
  pango_layout_context_changed (self->layout);

  update_json_from_layout (self, self->layout);
}

static void
tabs_changed (GtkTextBuffer *buffer,
              gpointer       data)
{
  LayoutEditor *self = data;
  GtkTextIter start, end;
  char *text;
  PangoTabArray *tabs;

  if (!self->layout)
    return;

  gtk_text_buffer_get_bounds (buffer, &start, &end);
  text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
  tabs = pango_tab_array_from_string (text);
  pango_layout_set_tabs (self->layout, tabs);
  if (tabs)
    pango_tab_array_free (tabs);
  g_free (text);

  update_json_from_layout (self, self->layout);
}

static void
font_changed (GtkWidget  *widget,
              GParamSpec *pspec,
              gpointer    data)
{
  LayoutEditor *self = data;
  const PangoFontDescription *desc;

  if (!self->layout)
    return;

  desc = gtk_font_chooser_get_font_desc (GTK_FONT_CHOOSER (self->font_editor));
  pango_layout_set_font_description (self->layout, desc);

  update_json_from_layout (self, self->layout);
}

static void
indent_changed (GtkWidget  *widget,
                GParamSpec *pspec,
                gpointer    data)
{
  LayoutEditor *self = data;
  int indent;

  if (!self->layout)
    return;

  indent = size_editor_get_value (self->indent_editor);
  pango_layout_set_indent (self->layout, indent);

  update_json_from_layout (self, self->layout);
}

static void
width_changed (GtkWidget  *widget,
               GParamSpec *pspec,
               gpointer    data)
{
  LayoutEditor *self = data;
  int width;

  if (!self->layout)
    return;

  width = size_editor_get_value (self->width_editor);
  pango_layout_set_width (self->layout, width);

  update_json_from_layout (self, self->layout);
}

static void
height_changed (GtkWidget  *widget,
                GParamSpec *pspec,
                gpointer    data)
{
  LayoutEditor *self = data;
  int height;

  if (!self->layout)
    return;

  height = size_editor_get_value (self->height_editor);
  pango_layout_set_height (self->layout, height);

  update_json_from_layout (self, self->layout);
}

static void
justify_changed (GtkWidget  *widget,
                 GParamSpec *pspec,
                 gpointer    data)
{
  LayoutEditor *self = data;

  if (!self->layout)
    return;

  pango_layout_set_justify (self->layout, gtk_switch_get_active (self->justify_editor));

  update_json_from_layout (self, self->layout);
}

static void
justify_last_line_changed (GtkWidget  *widget,
                           GParamSpec *pspec,
                           gpointer    data)
{
  LayoutEditor *self = data;

  if (!self->layout)
    return;

  pango_layout_set_justify_last_line (self->layout, gtk_switch_get_active (self->justify_last_line_editor));

  update_json_from_layout (self, self->layout);
}

static void
single_paragraph_changed (GtkWidget  *widget,
                          GParamSpec *pspec,
                          gpointer    data)
{
  LayoutEditor *self = data;

  if (!self->layout)
    return;

  pango_layout_set_single_paragraph_mode (self->layout, gtk_switch_get_active (self->single_paragraph_editor));

  update_json_from_layout (self, self->layout);
}

static void
auto_dir_changed (GtkWidget  *widget,
                  GParamSpec *pspec,
                  gpointer    data)
{
  LayoutEditor *self = data;

  if (!self->layout)
    return;

  pango_layout_set_auto_dir (self->layout, gtk_switch_get_active (self->auto_dir_editor));

  update_json_from_layout (self, self->layout);
}

static void
alignment_changed (GtkWidget  *widget,
                   GParamSpec *pspec,
                   gpointer    data)
{
  LayoutEditor *self = data;
  PangoAlignment alignment;

  if (!self->layout)
    return;

  alignment = gtk_drop_down_get_selected (self->alignment_editor);
  pango_layout_set_alignment (self->layout, alignment);

  update_json_from_layout (self, self->layout);
}

static void
wrap_changed (GtkWidget  *widget,
              GParamSpec *pspec,
              gpointer    data)
{
  LayoutEditor *self = data;
  PangoWrapMode wrap;

  if (!self->layout)
    return;

  wrap = gtk_drop_down_get_selected (self->wrap_editor);
  pango_layout_set_wrap (self->layout, wrap);

  update_json_from_layout (self, self->layout);
}

static void
ellipsize_changed (GtkWidget  *widget,
                   GParamSpec *pspec,
                   gpointer    data)
{
  LayoutEditor *self = data;
  PangoEllipsizeMode ellipsize;

  if (!self->layout)
    return;

  ellipsize = gtk_drop_down_get_selected (self->ellipsize_editor);
  pango_layout_set_ellipsize (self->layout, ellipsize);

  update_json_from_layout (self, self->layout);
}

static void
spacing_changed (GtkWidget  *widget,
                 GParamSpec *pspec,
                 gpointer    data)
{
  LayoutEditor *self = data;
  int spacing;

  if (!self->layout)
    return;

  spacing = size_editor_get_value (self->spacing_editor);
  pango_layout_set_spacing (self->layout, spacing);

  update_json_from_layout (self, self->layout);
}

static void
line_spacing_changed (GtkWidget  *widget,
                      GParamSpec *pspec,
                      gpointer    data)
{
  LayoutEditor *self = data;
  double line_spacing;

  if (!self->layout)
    return;

  line_spacing = gtk_spin_button_get_value (self->line_spacing_editor);
  pango_layout_set_line_spacing (self->layout, line_spacing);

  update_json_from_layout (self, self->layout);
}

static void
update_layout_from_layout (LayoutEditor *self,
                           PangoLayout  *src)
{
  PangoLayout *dest = self->layout;
  PangoTabArray *tabs;
  const char *str;

  if (!dest)
    return;

  str = (const char *) g_object_get_data (G_OBJECT (src), "comment");
  if (str)
    g_object_set_data_full (G_OBJECT (dest), "comment", g_strdup (str), g_free);
  pango_layout_set_text (dest, pango_layout_get_text (src), -1);
  pango_layout_set_attributes (dest, pango_layout_get_attributes (src));
  tabs = pango_layout_get_tabs (src);
  pango_layout_set_tabs (dest, tabs);
  if (tabs)
    pango_tab_array_free (tabs);
  pango_layout_set_font_description (dest, pango_layout_get_font_description (src));
  pango_layout_set_justify (dest, pango_layout_get_justify (src));
  pango_layout_set_justify_last_line (dest, pango_layout_get_justify_last_line (src));
  pango_layout_set_single_paragraph_mode (dest, pango_layout_get_single_paragraph_mode (src));
  pango_layout_set_auto_dir (dest, pango_layout_get_auto_dir (src));
  pango_layout_set_alignment (dest, pango_layout_get_alignment (src));
  pango_layout_set_wrap (dest, pango_layout_get_wrap (src));
  pango_layout_set_ellipsize (dest, pango_layout_get_ellipsize (src));
  pango_layout_set_width (dest, pango_layout_get_width (src));
  pango_layout_set_height (dest, pango_layout_get_height (src));
  pango_layout_set_indent (dest, pango_layout_get_indent (src));
  pango_layout_set_spacing (dest, pango_layout_get_spacing (src));
  pango_layout_set_line_spacing (dest, pango_layout_get_line_spacing (src));
}

static gboolean
get_error_location (const char  *msg,
                    gsize       *start,
                    gsize       *end,
                    char       **json_error)
{
  char *endptr = NULL;

  *start = g_ascii_strtoll (msg, &endptr, 10);
  if (!endptr || *endptr != ':')
    return FALSE;

  msg = endptr + 1;
  endptr = NULL;

  *end = g_ascii_strtoll (msg, &endptr, 10);
  if (!endptr || *endptr != ':')
    return FALSE;

  if (*end == *start)
    (*end)++;

  *json_error = g_strdup (endptr + 2);

  return TRUE;
}

static void
json_changed (GtkTextBuffer *buffer,
              gpointer       data)
{
  LayoutEditor *self = data;
  GtkTextIter start, end;
  char *text;
  GBytes *bytes;
  GError *error = NULL;
  PangoContext *context;
  PangoLayout *layout;

  if (!self->layout)
    return;

  self->skip_json_update = TRUE;

  gtk_text_buffer_get_bounds (buffer, &start, &end);
  text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
  bytes = g_bytes_new_take (text, strlen (text));
  context = gtk_widget_get_pango_context (GTK_WIDGET (self));
  layout = pango_layout_deserialize (context,
                                     bytes,
                                     PANGO_LAYOUT_DESERIALIZE_DEFAULT,
                                     &error);

  if (!layout)
    {
      gsize start, end;
      char *json_error = NULL;

      if (get_error_location (error->message, &start, &end, &json_error))
        {
          GtkTextIter start_iter;
          GtkTextIter end_iter;

          self->json_error_start = g_utf8_pointer_to_offset (text, text + start);
          self->json_error_end = self->json_error_start +
                                 g_utf8_pointer_to_offset (text + start, text + end);

          gtk_text_buffer_get_iter_at_offset (buffer, &start_iter, self->json_error_start);
          gtk_text_buffer_get_iter_at_offset (buffer, &end_iter, self->json_error_end);
          gtk_text_buffer_apply_tag_by_name (buffer, "error", &start_iter, &end_iter);

          g_free (self->json_error);
          self->json_error = json_error;
        }

      g_error_free (error);
    }
  else
    {
      GtkTextIter start, end;

      gtk_text_buffer_get_bounds (buffer, &start, &end);
      gtk_text_buffer_remove_all_tags (buffer, &start, &end);
      update_layout_from_layout (self, layout);
      update_editor_from_layout (self, layout);
      g_object_unref (layout);

      g_clear_pointer (&self->json_error, g_free);
      self->json_error_start = 0;
      self->json_error_end = 0;
    }

  g_bytes_unref (bytes);

  self->skip_json_update = FALSE;
}

static gboolean
drag_update_cb (GtkGestureDrag *gesture,
                double          offset_x,
                double          offset_y,
                LayoutEditor   *self)
{
  GtkWidget *handle, *sw;
  double start_x, start_y;
  double x, y;

  handle = gtk_event_controller_get_widget (GTK_EVENT_CONTROLLER (gesture));
  if (strcmp (gtk_event_controller_get_name (GTK_EVENT_CONTROLLER (gesture)), "text_handle") == 0)
    sw = gtk_widget_get_ancestor (GTK_WIDGET (self->text_editor), GTK_TYPE_SCROLLED_WINDOW);
  else if (strcmp (gtk_event_controller_get_name (GTK_EVENT_CONTROLLER (gesture)), "attr_handle") == 0)
    sw = gtk_widget_get_ancestor (GTK_WIDGET (self->attrs_editor), GTK_TYPE_SCROLLED_WINDOW);
  else if (strcmp (gtk_event_controller_get_name (GTK_EVENT_CONTROLLER (gesture)), "tabs_handle") == 0)
    sw = gtk_widget_get_ancestor (GTK_WIDGET (self->tabs_editor), GTK_TYPE_SCROLLED_WINDOW);
  else
    sw = gtk_widget_get_ancestor (GTK_WIDGET (self->comment_editor), GTK_TYPE_SCROLLED_WINDOW);
  gtk_gesture_drag_get_start_point (gesture, &start_x, &start_y);

  gtk_widget_translate_coordinates (handle, sw,
                                    start_x + offset_x,
                                    start_y + offset_y,
                                    &x, &y);

  gtk_scrolled_window_set_min_content_width (GTK_SCROLLED_WINDOW (sw), x);
  gtk_scrolled_window_set_min_content_height (GTK_SCROLLED_WINDOW (sw), y);

  return TRUE;
}

static gboolean
json_query_tooltip_cb (GtkWidget    *widget,
                       int           x,
                       int           y,
                       gboolean      keyboard_tip,
                       GtkTooltip   *tooltip,
                       LayoutEditor *self)
{
  GtkTextBuffer *buffer;
  GtkTextIter iter;
  GtkTextIter start, end;

  if (!self->json_error)
    return FALSE;

  buffer = gtk_text_view_get_buffer (self->json_editor);

  if (keyboard_tip)
    {
      int offset;

      g_object_get (buffer, "cursor-position", &offset, NULL);
      gtk_text_buffer_get_iter_at_offset (buffer, &iter, offset);
    }
  else
    {
      int bx, by, trailing;

      gtk_text_view_window_to_buffer_coords (self->json_editor, GTK_TEXT_WINDOW_TEXT, x, y, &bx, &by);
      gtk_text_view_get_iter_at_position (self->json_editor, &iter, &trailing, bx, by);
    }

  gtk_text_buffer_get_iter_at_offset (buffer, &start, self->json_error_start);
  gtk_text_buffer_get_iter_at_offset (buffer, &end, self->json_error_end);

  if (gtk_text_iter_in_range (&iter, &start, &end))
    {
      gtk_tooltip_set_text (tooltip, self->json_error);
      return TRUE;
    }

  return FALSE;
}

/*  }}} */
/* {{{ GObject boilerplate */

enum {
  PROP_JSON_MODE = 1,
  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL };

G_DEFINE_TYPE (LayoutEditor, layout_editor, GTK_TYPE_WIDGET);

static void
layout_editor_init (LayoutEditor *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
layout_editor_finalize (GObject *object)
{
  LayoutEditor *self = (LayoutEditor *)object;

  g_clear_pointer (&self->box, gtk_widget_unparent);
  g_clear_object (&self->layout);
  g_clear_pointer (&self->json_error, g_free);

  G_OBJECT_CLASS (layout_editor_parent_class)->finalize (object);
}

static void
layout_editor_dispose (GObject *object)
{
  LayoutEditor *self = (LayoutEditor *)object;

  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (layout_editor_parent_class)->dispose (object);
}

static void
layout_editor_set_property (GObject      *object,
                            guint         property_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  LayoutEditor *self = (LayoutEditor *)object;

  switch (property_id)
    {
    case PROP_JSON_MODE:
      layout_editor_set_json_mode (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
layout_editor_get_property (GObject    *object,
                            guint       property_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  LayoutEditor *self = (LayoutEditor *)object;

  switch (property_id)
    {
    case PROP_JSON_MODE:
      g_value_set_boolean (value, self->json_mode);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
layout_editor_class_init (LayoutEditorClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  g_type_ensure (size_editor_get_type ());

  object_class->finalize = layout_editor_finalize;
  object_class->dispose = layout_editor_dispose;
  object_class->set_property = layout_editor_set_property;
  object_class->get_property = layout_editor_get_property;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/pango/LayoutEditor/layout-editor.ui");

  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, box);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, comment_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, text_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, attrs_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, tabs_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, font_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, indent_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, width_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, height_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, justify_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, justify_last_line_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, single_paragraph_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, auto_dir_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, alignment_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, wrap_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, ellipsize_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, spacing_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, line_spacing_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, json_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditor, stack);

  gtk_widget_class_bind_template_callback (widget_class, comment_changed);
  gtk_widget_class_bind_template_callback (widget_class, text_changed);
  gtk_widget_class_bind_template_callback (widget_class, attrs_changed);
  gtk_widget_class_bind_template_callback (widget_class, tabs_changed);
  gtk_widget_class_bind_template_callback (widget_class, font_changed);
  gtk_widget_class_bind_template_callback (widget_class, indent_changed);
  gtk_widget_class_bind_template_callback (widget_class, width_changed);
  gtk_widget_class_bind_template_callback (widget_class, height_changed);
  gtk_widget_class_bind_template_callback (widget_class, justify_changed);
  gtk_widget_class_bind_template_callback (widget_class, justify_last_line_changed);
  gtk_widget_class_bind_template_callback (widget_class, single_paragraph_changed);
  gtk_widget_class_bind_template_callback (widget_class, auto_dir_changed);
  gtk_widget_class_bind_template_callback (widget_class, alignment_changed);
  gtk_widget_class_bind_template_callback (widget_class, wrap_changed);
  gtk_widget_class_bind_template_callback (widget_class, ellipsize_changed);
  gtk_widget_class_bind_template_callback (widget_class, spacing_changed);
  gtk_widget_class_bind_template_callback (widget_class, line_spacing_changed);
  gtk_widget_class_bind_template_callback (widget_class, json_changed);
  gtk_widget_class_bind_template_callback (widget_class, drag_update_cb);
  gtk_widget_class_bind_template_callback (widget_class, json_query_tooltip_cb);

  properties[PROP_JSON_MODE] =
    g_param_spec_boolean  ("json-mode", NULL, NULL,
                           FALSE,
                           G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);

  gtk_widget_class_set_css_name (widget_class, "layout-editor");
}

 /* }}} */
/* {{{ Public API */

LayoutEditor *
layout_editor_new (void)
{
  return g_object_new (layout_editor_get_type (), NULL);
}

void
layout_editor_set_layout (LayoutEditor *self,
                          PangoLayout  *layout)
{
  g_clear_object (&self->layout);
  update_editor_from_layout (self, layout);
  update_json_from_layout (self, layout);
  self->layout = layout;
  if (self->layout)
    g_object_ref (self->layout);
}

gboolean
layout_editor_get_json_mode (LayoutEditor *self)
{
  return self->json_mode;
}

void
layout_editor_set_json_mode (LayoutEditor *self,
                             gboolean      json_mode)
{
  if (self->json_mode == json_mode)
    return;

  self->json_mode = json_mode;

  gtk_stack_set_visible_child_name (GTK_STACK (self->stack),
                                    json_mode ? "json" : "editor");

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_JSON_MODE]);
}

/* }}} */

/* vim:set foldmethod=marker expandtab: */
