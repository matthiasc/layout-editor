/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#pragma once

#include <gtk/gtk.h>

#include "layout-editor-application.h"

G_DECLARE_FINAL_TYPE (LayoutEditorWindow, layout_editor_window, LAYOUT_EDITOR, WINDOW, GtkApplicationWindow)

LayoutEditorWindow *    layout_editor_window_new          (LayoutEditorApplication  *application);

gboolean                layout_editor_window_load         (LayoutEditorWindow       *self,
                                                           GFile                    *file);

void                    layout_editor_window_set_fonts    (LayoutEditorWindow       *self,
                                                           GFile                    *dir);
