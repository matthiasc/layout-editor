/*
 * Copyright 2021 Matthias Clasen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Matthias Clasen
 */

#include "config.h"

#include "layout-editor-window.h"
#include "layout-editor.h"
#include "context-editor.h"
#include "layout-viewer.h"
#include <pango/pangocairo.h>
#include <pango/pangofc-fontmap.h>


struct _LayoutEditorWindow
{
  GtkApplicationWindow parent;

  LayoutEditor *layout_editor;
  ContextEditor *context_editor;
  LayoutViewer *layout_viewer;
  GtkToggleButton *layout_button;
  GtkToggleButton *context_button;
  GtkToggleButton *rendering_button;
  GtkRevealer *sidebar_revealer;
  GtkStack *sidebar_stack;
  GtkWidget *save_button;

  PangoFontMap *fontmap;
  PangoContext *context;
  PangoLayout *layout;

  guint tick_cb;
  guint serial;

  GFile *file;
  GFileMonitor *file_monitor;
};

struct _LayoutEditorWindowClass
{
  GtkApplicationWindowClass parent_class;
};

/* {{{ LayoutEditorWindow implementation */

static gboolean
tick_cb (GtkWidget     *widget,
         GdkFrameClock *clock,
         gpointer       user_data)
{
  LayoutEditorWindow *self = (LayoutEditorWindow *)widget;
  guint serial;

  serial = pango_layout_get_serial (self->layout);
  if (self->serial != serial)
    {
      gtk_widget_set_sensitive (self->save_button, TRUE);
      self->tick_cb = 0;

      return G_SOURCE_REMOVE;
    }

  return G_SOURCE_CONTINUE;
}

static void
layout_editor_window_set_layout (LayoutEditorWindow *self,
                                 PangoLayout        *layout)
{
  if (self->layout)
    g_object_unref (self->layout);
  self->layout = layout;
  if (self->layout)
    g_object_ref (self->layout);

  self->serial = pango_layout_get_serial (self->layout);
  gtk_widget_set_sensitive (self->save_button, FALSE);

  if (!self->tick_cb)
    self->tick_cb = gtk_widget_add_tick_callback (GTK_WIDGET (self), tick_cb, NULL, NULL);

  layout_editor_set_layout (self->layout_editor, self->layout);
  context_editor_set_context (self->context_editor, pango_layout_get_context (self->layout));
  layout_viewer_set_layout (self->layout_viewer, self->layout);
}

static gboolean
load_bytes (LayoutEditorWindow *self,
            GBytes             *bytes)
{
  PangoLayout *layout;
  GError *error = NULL;

  layout = pango_layout_deserialize (self->context,
                                     bytes,
                                     PANGO_LAYOUT_DESERIALIZE_CONTEXT,
                                     &error);

  if (layout)
    {
      layout_editor_window_set_layout (self, layout);
      g_object_unref (layout);
    }
  else
    {
      g_warning ("ERROR: %s", error->message);
      g_error_free (error);
    }

  g_bytes_unref (bytes);

  return TRUE;
}

static gboolean
load_file_contents (LayoutEditorWindow *self,
                    GFile              *file)
{
  GBytes *bytes;
  char *title;

  bytes = g_file_load_bytes (file, NULL, NULL, NULL);
  if (bytes == NULL)
    return FALSE;

  if (!load_bytes (self, bytes))
    return FALSE;

  title = g_file_get_basename (file);
  gtk_window_set_title (GTK_WINDOW (self), title);
  g_free (title);

  return TRUE;
}

static void
file_changed_cb (GFileMonitor      *monitor,
                 GFile             *file,
                 GFile             *other_file,
                 GFileMonitorEvent  event_type,
                 gpointer           user_data)
{
  LayoutEditorWindow *self = user_data;

  if (event_type == G_FILE_MONITOR_EVENT_CHANGED)
    load_file_contents (self, file);
}

static void
open_response_cb (GtkWidget          *dialog,
                  int                 response,
                  LayoutEditorWindow *self)
{
  gtk_widget_hide (dialog);

  if (response == GTK_RESPONSE_ACCEPT)
    {
      GFile *file;

      file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (dialog));
      layout_editor_window_load (self, file);
      g_object_unref (file);
    }

  gtk_window_destroy (GTK_WINDOW (dialog));
}

static void
show_open_filechooser (LayoutEditorWindow *self)
{
  GtkWidget *dialog;

  dialog = gtk_file_chooser_dialog_new ("Open layout file",
                                        GTK_WINDOW (self),
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        "_Cancel", GTK_RESPONSE_CANCEL,
                                        "_Load", GTK_RESPONSE_ACCEPT,
                                        NULL);

  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);

  GFile *cwd = g_file_new_for_path (".");
  gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), cwd, NULL);
  g_object_unref (cwd);

  g_signal_connect (dialog, "response", G_CALLBACK (open_response_cb), self);
  gtk_widget_show (dialog);
}

static void
open_cb (GtkWidget          *button,
         LayoutEditorWindow *self)
{
  show_open_filechooser (self);
}

static void
save_response_cb (GtkNativeDialog    *dialog,
                  int                 response,
                  LayoutEditorWindow *self)
{
  gtk_native_dialog_hide (dialog);

  if (response == GTK_RESPONSE_ACCEPT)
    {
      GFile *file;
      GBytes *bytes;
      GError *error = NULL;

      bytes = pango_layout_serialize (self->layout, PANGO_LAYOUT_SERIALIZE_CONTEXT | PANGO_LAYOUT_SERIALIZE_OUTPUT);

      file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (dialog));
      g_file_replace_contents (file,
                               (char *)g_bytes_get_data (bytes, NULL),
                               g_bytes_get_size (bytes),
                               NULL, FALSE,
                               G_FILE_CREATE_NONE,
                               NULL,
                               NULL,
                               &error);
      if (error != NULL)
        {
          GtkWidget *message_dialog;

          message_dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_root (GTK_WIDGET (self))),
                                                   GTK_DIALOG_MODAL|GTK_DIALOG_DESTROY_WITH_PARENT,
                                                   GTK_MESSAGE_INFO,
                                                   GTK_BUTTONS_OK,
                                                   "Saving failed");
          gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (message_dialog),
                                                    "%s", error->message);
          g_signal_connect (message_dialog, "response", G_CALLBACK (gtk_window_destroy), NULL);
          gtk_widget_show (message_dialog);
          g_error_free (error);
        }

      g_bytes_unref (bytes);
      g_object_unref (file);
    }

  gtk_native_dialog_destroy (GTK_NATIVE_DIALOG (dialog));
}

static void
save_cb (GtkWidget          *button,
         LayoutEditorWindow *self)
{
  GtkFileChooserNative *dialog;

  dialog = gtk_file_chooser_native_new ("Save layout",
                                        GTK_WINDOW (gtk_widget_get_root (GTK_WIDGET (button))),
                                        GTK_FILE_CHOOSER_ACTION_SAVE,
                                        "_Save",
                                        "_Cancel");

  gtk_native_dialog_set_modal (GTK_NATIVE_DIALOG (dialog), TRUE);

  if (self->file)
    {
      GFile *dir = g_file_get_parent (self->file);
      char *name = g_file_get_basename (self->file);

      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), dir, NULL);
      gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), name);
      g_object_unref (dir);
      g_free (name);
    }
  else
    {
      GFile *cwd = g_file_new_for_path (".");
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), cwd, NULL);
      g_object_unref (cwd);
    }

  g_signal_connect (dialog, "response", G_CALLBACK (save_response_cb), self);
  gtk_native_dialog_show (GTK_NATIVE_DIALOG (dialog));
}

static GdkTexture *
create_texture (LayoutEditorWindow *self)
{
  GdkPaintable *paintable;
  GtkSnapshot *snapshot;
  GskRenderNode *node;
  GskRenderer *renderer;
  GdkTexture *texture;

  paintable = gtk_widget_paintable_new (GTK_WIDGET (self->layout_viewer));
  snapshot = gtk_snapshot_new ();
  gdk_paintable_snapshot (paintable, snapshot,
                          gdk_paintable_get_intrinsic_width (paintable),
                          gdk_paintable_get_intrinsic_height (paintable));
  node = gtk_snapshot_free_to_node (snapshot);
  renderer = gtk_native_get_renderer (gtk_widget_get_native (GTK_WIDGET (self)));
  texture = gsk_renderer_render_texture (renderer, node, NULL);
  gsk_render_node_unref (node);

  g_object_unref (paintable);

  return texture;
}

static void
export_image_response_cb (GtkNativeDialog *dialog,
                          int              response,
                          GdkTexture      *texture)
{
  gtk_native_dialog_hide (dialog);

  if (response == GTK_RESPONSE_ACCEPT)
    {
      GFile *file;

      file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (dialog));
      if (!gdk_texture_save_to_png (texture, g_file_peek_path (file)))
        {
          GtkWidget *message_dialog;

          message_dialog = gtk_message_dialog_new (gtk_native_dialog_get_transient_for (dialog),
                                                   GTK_DIALOG_MODAL|GTK_DIALOG_DESTROY_WITH_PARENT,
                                                   GTK_MESSAGE_INFO,
                                                   GTK_BUTTONS_OK,
                                                   "Exporting to image failed");
          g_signal_connect (message_dialog, "response", G_CALLBACK (gtk_window_destroy), NULL);
          gtk_widget_show (message_dialog);
        }

      g_object_unref (file);
    }

  gtk_native_dialog_destroy (dialog);
  g_object_unref (texture);
}

static void
export_image_cb (GtkButton          *button,
                 LayoutEditorWindow *self)
{
  GdkTexture *texture;
  GtkFileChooserNative *dialog;

  texture = create_texture (self);
  if (texture == NULL)
    return;

  dialog = gtk_file_chooser_native_new ("Export Image",
                                        GTK_WINDOW (gtk_widget_get_root (GTK_WIDGET (button))),
                                        GTK_FILE_CHOOSER_ACTION_SAVE,
                                        "_Save",
                                        "_Cancel");

  gtk_native_dialog_set_modal (GTK_NATIVE_DIALOG (dialog), TRUE);

  if (self->file)
    {
      GFile *dir = g_file_get_parent (self->file);
      char *name = g_file_get_basename (self->file);

      if (g_str_has_suffix (name, ".layout"))
        {
          name[strlen (name) - strlen (".layout")] = '\0';
          strcat (name, ".png");
        }
      else
        {
          char *p = g_strconcat (name, ".png", NULL);
          g_free (name);
          name = p;
        }

      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), dir, NULL);
      gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), name);
      g_object_unref (dir);
      g_free (name);
    }
  else
    {
      GFile *cwd = g_file_new_for_path (".");
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), cwd, NULL);
      g_object_unref (cwd);
    }

  g_signal_connect (dialog, "response", G_CALLBACK (export_image_response_cb), texture);
  gtk_native_dialog_show (GTK_NATIVE_DIALOG (dialog));
}

static void
dark_mode_cb (GtkToggleButton    *button,
              GParamSpec         *pspec,
              LayoutEditorWindow *self)
{
  g_object_set (gtk_widget_get_settings (GTK_WIDGET (self)),
                "gtk-application-prefer-dark-theme", gtk_toggle_button_get_active (button),
                NULL);
}

static void
layout_cb (GtkToggleButton    *button,
           GParamSpec         *pspec,
           LayoutEditorWindow *self)
{
  if (gtk_toggle_button_get_active (self->layout_button))
    gtk_stack_set_visible_child_name (self->sidebar_stack, "layout");

  if (gtk_toggle_button_get_active (self->layout_button) &&
      (gtk_toggle_button_get_active (self->context_button) ||
       gtk_toggle_button_get_active (self->rendering_button)))
    {
      gtk_toggle_button_set_active (self->context_button, FALSE);
      gtk_toggle_button_set_active (self->rendering_button, FALSE);
    }
  else if (!gtk_toggle_button_get_active (self->layout_button) &&
           !gtk_toggle_button_get_active (self->context_button) &&
           !gtk_toggle_button_get_active (self->rendering_button))
    {
      gtk_revealer_set_reveal_child (self->sidebar_revealer, FALSE);
    }
  else
    {
      gtk_revealer_set_reveal_child (self->sidebar_revealer, TRUE);
    }
}

static void
context_cb (GtkToggleButton    *button,
            GParamSpec         *pspec,
            LayoutEditorWindow *self)
{
  if (gtk_toggle_button_get_active (self->context_button))
    gtk_stack_set_visible_child_name (self->sidebar_stack, "context");

  if (gtk_toggle_button_get_active (self->context_button) &&
      (gtk_toggle_button_get_active (self->layout_button) ||
       gtk_toggle_button_get_active (self->rendering_button)))
    {
      gtk_toggle_button_set_active (self->layout_button, FALSE);
      gtk_toggle_button_set_active (self->rendering_button, FALSE);
    }
  else if (!gtk_toggle_button_get_active (self->layout_button) &&
           !gtk_toggle_button_get_active (self->context_button) &&
           !gtk_toggle_button_get_active (self->rendering_button))
    {
      gtk_revealer_set_reveal_child (self->sidebar_revealer, FALSE);
    }
  else
    {
      gtk_revealer_set_reveal_child (self->sidebar_revealer, TRUE);
    }
}

static void
rendering_cb (GtkToggleButton    *button,
              GParamSpec         *pspec,
              LayoutEditorWindow *self)
{
  if (gtk_toggle_button_get_active (self->rendering_button))
    gtk_stack_set_visible_child_name (self->sidebar_stack, "rendering");

  if (gtk_toggle_button_get_active (self->rendering_button) &&
      (gtk_toggle_button_get_active (self->layout_button) ||
       gtk_toggle_button_get_active (self->context_button)))
    {
      gtk_toggle_button_set_active (self->context_button, FALSE);
      gtk_toggle_button_set_active (self->layout_button, FALSE);
    }
  else if (!gtk_toggle_button_get_active (self->layout_button) &&
           !gtk_toggle_button_get_active (self->context_button) &&
           !gtk_toggle_button_get_active (self->rendering_button))
    {
      gtk_revealer_set_reveal_child (self->sidebar_revealer, FALSE);
    }
  else
    {
      gtk_revealer_set_reveal_child (self->sidebar_revealer, TRUE);
    }
}

 /* }}} */
/* {{{ GObject boilerplate */

G_DEFINE_TYPE (LayoutEditorWindow, layout_editor_window, GTK_TYPE_APPLICATION_WINDOW);

static void
window_open (GSimpleAction *action,
             GVariant      *parameter,
             gpointer       user_data)
{
  LayoutEditorWindow *self = user_data;

  show_open_filechooser (self);
}

static GActionEntry win_entries[] = {
  { "open", window_open, NULL, NULL, NULL },
};

static void
layout_editor_window_init (LayoutEditorWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");

  g_action_map_add_action_entries (G_ACTION_MAP (self), win_entries, G_N_ELEMENTS (win_entries), self);

  self->fontmap = g_object_ref (pango_cairo_font_map_get_default ());
  self->context = pango_font_map_create_context (self->fontmap);

  load_bytes (self, g_resources_lookup_data ("/org/pango/LayoutEditor/default.layout", 0, NULL));
}

static void
layout_editor_window_finalize (GObject *object)
{
  LayoutEditorWindow *self = (LayoutEditorWindow *)object;

  g_clear_object (&self->context);
  g_clear_object (&self->layout);
  g_clear_object (&self->file);
  g_clear_object (&self->file_monitor);
  g_clear_object (&self->fontmap);

  G_OBJECT_CLASS (layout_editor_window_parent_class)->finalize (object);
}

static void
layout_editor_window_realize (GtkWidget *widget)
{
  //LayoutEditorWindow *self = LAYOUT_EDITOR_WINDOW (widget);

  GTK_WIDGET_CLASS (layout_editor_window_parent_class)->realize (widget);
}

static void
layout_editor_window_unrealize (GtkWidget *widget)
{
  //LayoutEditorWindow *self = LAYOUT_EDITOR_WINDOW (widget);

  GTK_WIDGET_CLASS (layout_editor_window_parent_class)->unrealize (widget);
}

static void
layout_editor_window_class_init (LayoutEditorWindowClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  g_type_ensure (layout_editor_get_type ());
  g_type_ensure (context_editor_get_type ());
  g_type_ensure (layout_viewer_get_type ());

  object_class->finalize = layout_editor_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/pango/LayoutEditor/layout-editor-window.ui");

  widget_class->realize = layout_editor_window_realize;
  widget_class->unrealize = layout_editor_window_unrealize;

  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, layout_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, context_editor);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, layout_viewer);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, layout_button);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, context_button);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, rendering_button);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, sidebar_revealer);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, sidebar_stack);
  gtk_widget_class_bind_template_child (widget_class, LayoutEditorWindow, save_button);

  gtk_widget_class_bind_template_callback (widget_class, open_cb);
  gtk_widget_class_bind_template_callback (widget_class, save_cb);
  gtk_widget_class_bind_template_callback (widget_class, export_image_cb);
  gtk_widget_class_bind_template_callback (widget_class, dark_mode_cb);
  gtk_widget_class_bind_template_callback (widget_class, layout_cb);
  gtk_widget_class_bind_template_callback (widget_class, context_cb);
  gtk_widget_class_bind_template_callback (widget_class, rendering_cb);
}

/* }}} */
/* {{{ Public API */

LayoutEditorWindow *
layout_editor_window_new (LayoutEditorApplication *application)
{
  return g_object_new (layout_editor_window_get_type (),
                       "application", application,
                       NULL);
}

gboolean
layout_editor_window_load (LayoutEditorWindow *self,
                           GFile              *file)
{
  GError *error = NULL;

  if (!load_file_contents (self, file))
    return FALSE;

  g_clear_object (&self->file);
  self->file = g_object_ref (file);

  g_clear_object (&self->file_monitor);
  self->file_monitor = g_file_monitor_file (file, G_FILE_MONITOR_NONE, NULL, &error);

  if (error)
    {
      g_warning ("couldn't monitor file: %s", error->message);
      g_error_free (error);
    }
  else
    {
      g_signal_connect (self->file_monitor, "changed", G_CALLBACK (file_changed_cb), self);
    }

  return TRUE;
}

void
layout_editor_window_set_fonts (LayoutEditorWindow *self,
                                GFile              *dir)
{
  GType type;

  type = G_OBJECT_TYPE (self->fontmap);

  g_clear_object (&self->fontmap);

  if (dir)
    {
      FcConfig *config;
      GFile *file;

      config = FcConfigCreate ();

      file = g_file_get_child (dir, "fonts.conf");
      if (g_file_query_exists (file, NULL))
        FcConfigParseAndLoad (config, (const FcChar8 *) g_file_peek_path (file), TRUE);

      self->fontmap = g_object_new (type, NULL);
      FcConfigAppFontAddDir (config, (const FcChar8 *) g_file_peek_path (dir));
      pango_fc_font_map_set_config (PANGO_FC_FONT_MAP (self->fontmap), config);

      g_object_unref (file);

      FcConfigDestroy (config);
    }

  if (!self->fontmap)
    self->fontmap = g_object_ref (pango_cairo_font_map_get_default ());

  pango_context_set_font_map (self->context, self->fontmap);
}

/* }}} */

/* vim:set foldmethod=marker expandtab: */
